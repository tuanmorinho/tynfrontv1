import {Component, DestroyRef, inject, OnDestroy} from '@angular/core';
import {Observable, Subject, takeUntil} from "rxjs";
import {IDeviceInfo, RequestSeoMeta} from "@core/models/core";
import {AppState} from "@core/store/app.state";
import {select, Store} from "@ngrx/store";
import {selectDevice} from "@core/store/device/device.selector";
import {updateMHeaderConfig} from "@core/store/m-header-config/m-header-config.actions";
import {MHeaderConfigState} from "@core/store/m-header-config/m-header-config.state";
import {SeoService} from "@core/services/core/seo.service";
import {SeoMeta} from "@core/models/core";
import {LoadingState} from "@core/store/loading/loading.state";
import {setLoading, setSkeleton} from "@core/store/loading/loading.actions";
import {loading, skeleton} from "@core/store/loading/loading.selector";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {LayoutState} from "@core/store/layout/layout.state";
import {updateLayout} from "@core/store/layout/layout.actions";

@Component({
  selector: 'app-base',
  template: `<div></div>`
})
export class BaseComponent {
  private _destroy$: Subject<void> = new Subject<void>();

  private seoService: SeoService = inject<SeoService>(SeoService);
  private destroyRef: DestroyRef = inject<DestroyRef>(DestroyRef);
  public router: Router = inject<Router>(Router);
  public store: Store<AppState> = inject<Store<AppState>>(Store<AppState>);

  public deviceInfo$: Observable<IDeviceInfo> = this.store.pipe(select(selectDevice), this.onDestroy<IDeviceInfo>());
  public loading$: Observable<LoadingState | undefined> = this.store.pipe(select(loading), this.onDestroy<LoadingState | undefined>());
  public skeleton$: Observable<boolean | undefined> = this.store.pipe(select(skeleton), this.onDestroy<boolean | undefined>());

  protected get queryParams() {
    return this.activatedRoute.queryParams;
  }

  protected get routeParams(): Params {
    return this.activatedRoute.snapshot.params;
  }

  public updateSeoMetaTag(data: SeoMeta): void {
    this.seoService.updateSeoMeta(data);
  }

  public getSeoMetaTag(req?: RequestSeoMeta): void {
    this.seoService.getSeoMeta(req);
  }

  public configMobileHeader(payload: MHeaderConfigState): void {
    this.store.dispatch(updateMHeaderConfig({payload}));
  }

  public configLayout(payload: LayoutState): void {
    this.store.dispatch(updateLayout({payload}));
  }

  public set isLoading(config: boolean | LoadingState) {
    if (typeof config === 'boolean') {
      this.store.dispatch(setLoading({payload: { isLoading: config }}));
    }
    if (typeof config !== 'boolean') {
      if (!config?.overlay) {
        config.overlay = false;
      }
      this.store.dispatch(setLoading({payload: config}));
    }
  }

  public set skeletonVisible(visible: boolean) {
    this.store.dispatch(setSkeleton({payload: visible}));
  }

  public onDestroy<T>() {
    this.destroyRef.onDestroy(() => {
      this._destroy$.next();
      this._destroy$.complete();
    });
    return takeUntil<T>(this._destroy$);
  }

  private get activatedRoute(): ActivatedRoute {
    let route: ActivatedRoute = this.router.routerState.root;
    while (route.firstChild) {
      route = route.firstChild;
    }
    return route;
  }
}
