import {Component} from '@angular/core';
import {BaseComponent} from "@core/module/base/base.component";

@Component({
    selector: 'app-error-page',
    templateUrl: './error-page.component.html',
    styles: [`
        .w-logo {
          font-family: 'Ballpark', sans-serif !important;
          font-size: 60px;
          color: #505050;
          cursor: pointer;
        }

        .btn-err-back-to-home {
          padding: 12px 40px;
          border-radius: 30px;
          background: #87D1CD;
          transition: all .3s ease-in-out;
          width: 100%;
          color: #FFF;
          font-size: 16px;
          font-style: normal;
          font-weight: 400;
          line-height: normal;

          &:hover {
            transform: scale(0.9);
            background: #9FD19F;
          }
        }
    `]
})
export class ErrorPageComponent extends BaseComponent {

}
