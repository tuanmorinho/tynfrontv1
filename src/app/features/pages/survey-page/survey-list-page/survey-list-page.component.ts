import {Component, OnInit} from '@angular/core';
import {BaseComponent} from "@core/module/base/base.component";

@Component({
    selector: 'app-survey-list-page',
    templateUrl: './survey-list-page.component.html'
})
export class SurveyListPageComponent extends BaseComponent implements OnInit {
    ngOnInit(): void {
    }

}
