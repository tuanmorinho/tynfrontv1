import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MainComponent} from './main/main.component';
import {FooterComponent} from './components/footer/footer.component';
import {MNavigationComponent} from './components/m-navigation/m-navigation.component';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedModule} from "@shared/shared.module";
import {CoreModule} from "@core/module/core.module";
import {WNavigationComponent} from './components/w-navigation/w-navigation.component';
import {TuiAvatarModule, TuiInputModule} from "@taiga-ui/kit";
import {TuiButtonModule, TuiDataListModule, TuiDialogModule, TuiHostedDropdownModule} from "@taiga-ui/core";
import {TuiAutoFocusModule} from "@taiga-ui/cdk";


@NgModule({
    declarations: [
        MainComponent,
        FooterComponent,
        MNavigationComponent,
        WNavigationComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        FormsModule,
        SharedModule,
        CoreModule,
        TuiAvatarModule,
        TuiHostedDropdownModule,
        TuiButtonModule,
        TuiDataListModule,
        TuiDialogModule,
        ReactiveFormsModule,
        TuiInputModule,
        TuiAutoFocusModule
    ],
    exports: [
        MainComponent,
        FooterComponent,
        MNavigationComponent,
    ]
})
export class LayoutModule {
}
