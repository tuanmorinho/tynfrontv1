export interface DeviceState {
  deviceType?: string;
  isMobile?: boolean;
  isDesktop?: boolean;
}
