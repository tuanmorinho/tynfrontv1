import {Component, OnInit} from '@angular/core';
import {BaseComponent} from "@core/module/base/base.component";

@Component({
    selector: 'app-banner',
    templateUrl: './banner.component.html'
})
export class BannerComponent extends BaseComponent implements OnInit {
    index: number = 0;

    items: string[] = [
        'carousel-1.jpg',
        'carousel-2.jpg',
        'carousel-3.jpg',
    ]

    ngOnInit(): void {
    }
}
