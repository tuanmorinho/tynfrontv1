import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChild,
  ElementRef,
  inject, Input,
  OnInit, Renderer2,
  ViewChild,
} from '@angular/core';
import {BaseComponent} from "@core/module/base/base.component";
import {debounceTime, distinctUntilChanged, fromEvent, Observable} from "rxjs";
import {MHeaderConfigState} from "@core/store/m-header-config/m-header-config.state";
import {select} from "@ngrx/store";
import {selectMHeaderConfig} from "@core/store/m-header-config/m-header-config.selector";
import {MHeaderLeftDirective} from "@core/module/directive/m-header-left.directive";
import {MHeaderRightDirective} from "@core/module/directive/m-header-right.directive";
import {MHeaderCenterDirective} from "@core/module/directive/m-header-center.directive";

@Component({
  selector: 'app-m-header-transparent',
  templateUrl: './m-header-transparent.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MHeaderTransparentComponent extends BaseComponent implements OnInit, AfterViewInit {

  collapseHeader: boolean = false;
  rightHeaderLength: number = 0;
  gridCol: string = '';
  totalCartQty: number = 2;

  private cdr: ChangeDetectorRef = inject<ChangeDetectorRef>(ChangeDetectorRef);
  private renderer: Renderer2 = inject<Renderer2>(Renderer2);

  @ContentChild(MHeaderLeftDirective, {static: false}) mHeaderLeftContent!: MHeaderLeftDirective;
  @ContentChild(MHeaderRightDirective, {static: false}) mHeaderRightContent!: MHeaderRightDirective;
  @ContentChild(MHeaderCenterDirective, {static: false}) mHeaderCenterContent!: MHeaderCenterDirective;

  @ViewChild('rightHeaderEL', {static: false}) rightHeader!: ElementRef;

  @Input() backFn!: () => void;

  mHeaderConfig$: Observable<MHeaderConfigState | undefined> = this.store.pipe(select(selectMHeaderConfig), this.onDestroy<MHeaderConfigState | undefined>());

  ngOnInit(): void {
    this.listenScroll()
  }

  ngAfterViewInit(): void {
    this.updateGridStyle();
  }

  mBack(): void {
    if (typeof this.backFn === 'function') {
      this.backFn();
    } else {
      this.back();
    }
  }

  mToCart() {}

  private back() {
    if (window?.history?.length > 1) {
      window.history.back();
    } else {
      this.router.navigate(['']);
    }
  }

  private listenScroll() {
    fromEvent(window, 'scroll', {passive: true})
      .pipe(debounceTime(0), distinctUntilChanged(), this.onDestroy<any>()).subscribe((e) => {
        this.collapseHeader = window.scrollY > 0;
        this.cdr.detectChanges();
      }
    )
  }

  private updateGridStyle() {
    if (this.rightHeader && this.rightHeader?.nativeElement) {
      this.renderer.setStyle(this.rightHeader.nativeElement, 'display', 'grid');
      this.renderer.setStyle(this.rightHeader.nativeElement, 'grid-template-columns', `repeat(${this.rightHeader.nativeElement?.children?.length}, minmax(0, 1fr))`);
      this.cdr.detectChanges();
    }
  }
}
