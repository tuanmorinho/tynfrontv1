import {Component, inject, Input, OnInit} from '@angular/core';
import {BaseComponent} from "../../../../core/module/base/base.component";
import {Router} from "@angular/router";
import {debounceTime, Observable, of} from "rxjs";
import {select} from "@ngrx/store";
import {selectSearchKeyword} from "../../../../core/store/search/search.selector";
import {FormBuilder, FormGroup} from "@angular/forms";
import {switchMap} from "rxjs/operators";
import {setKeywordSearch} from "../../../../core/store/search/search.actions";
import {ROUTING_TYN} from "../../../../core/constants/routing-constant";

@Component({
  selector: 'app-m-header-search',
  templateUrl: './m-header-search.component.html'
})
export class MHeaderSearchComponent extends BaseComponent implements OnInit {

  @Input() focusable: boolean = false;

  private fb: FormBuilder = inject<FormBuilder>(FormBuilder);

  searchForm: FormGroup = this.fb.group({
    keyword: ['']
  });

  keyword$: Observable<string | undefined> = this.store.pipe(select(selectSearchKeyword), this.onDestroy<string | undefined>());

  ngOnInit() {
    if (this.focusable) this.listenKeyword();
    if (!this.focusable) this.formControl('keyword')?.disable();
  }

  get formValue() {
    return this.searchForm.value
  }

  formControl(control: string) {
    return this.searchForm.get(control);
  }

  private listenKeyword() {
    this.searchForm.valueChanges.pipe(
      this.onDestroy<typeof this.formValue>(),
      debounceTime(400),
      switchMap((data: typeof this.formValue) => {
        this.store.dispatch(setKeywordSearch({payload: data}));
        return of(null)
      })
    ).subscribe();
  }

  focusedChange(evt: any) {

  }

  private toSearch() {
    this.router.navigateByUrl('tim-kiem');
  }

}
