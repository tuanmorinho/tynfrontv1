import {Component, Input} from '@angular/core';
import {TUI_LOADER_OPTIONS} from "@taiga-ui/core";

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  providers: [
    {
      provide: TUI_LOADER_OPTIONS,
      useValue: {
        size: 'l',
        inheritColor: false,
      },
    },
  ]
})
export class LoadingComponent {
  @Input() showLoader: boolean = false;
  @Input() overlay: boolean = false;
}
