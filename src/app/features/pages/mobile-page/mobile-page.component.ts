import {Component} from '@angular/core';
import {BaseComponent} from "@core/module/base/base.component";

@Component({
    selector: 'app-mobile-page',
    templateUrl: './mobile-page.component.html',
    styles: [`
        .m-logo {
          font-family: 'Ballpark', sans-serif !important;
          font-size: 40px;
          color: #505050;
          cursor: pointer;
        }
    `]
})
export class MobilePageComponent extends BaseComponent {

}
