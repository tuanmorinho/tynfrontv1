import {Component, inject, OnInit} from '@angular/core';
import {BaseComponent} from "@core/module/base/base.component";
import {ActivatedRoute} from "@angular/router";
import {SurveyService} from "@core/services/features";
import {ISurvey} from "@core/models/features";
import {ROUTING_CONSTANT, ROUTING_TYN} from "@core/constants/routing-constant";
import {environment} from "../../../../../enviroments";

@Component({
    selector: 'app-survey-detail-page',
    templateUrl: './survey-detail-page.component.html'
})
export class SurveyDetailPageComponent extends BaseComponent implements OnInit {
    surveyId: number | undefined;
    survey: ISurvey | undefined;

    urlImageBase: string = environment.apiUrl.base.replace('/v2', '');

    private route: ActivatedRoute = inject<ActivatedRoute>(ActivatedRoute);
    private surveyService: SurveyService = inject<SurveyService>(SurveyService);

    ngOnInit(): void {
        if (this.route.snapshot.params['id']) {
            this.surveyId = +this.route.snapshot.params['id'];
            this.getSurvey();
        }
    }

    backToSurveyList(): void {
        if (this.survey && this.survey.surveyCategoryId) {
            void this.router.navigate([`${ROUTING_TYN.SURVEY}/${ROUTING_CONSTANT.LIST}/${this.survey.surveyCategoryId}`])
        }
    }

    toDoSurvey(): void {
        if (this.survey && this.survey.id) {
            void this.router.navigate([`${ROUTING_TYN.SURVEY}/${ROUTING_CONSTANT.QUIZ}/${this.survey.id}`])
        }
    }

    private getSurvey(): void {
        this.surveyService.getSurvey({id: this.surveyId}).subscribe({
            next: (data: ISurvey): void => {
                this.surveyId = data?.id;
                this.survey = data;
            }
        })
    }
}
