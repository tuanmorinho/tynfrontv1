import {Component, Input} from '@angular/core';
import {TUI_BUTTON_OPTIONS, TuiAppearance, TuiSizeXL, TuiSizeXS} from "@taiga-ui/core";

@Component({
  selector: 'app-button-primary',
  templateUrl: './button-primary.component.html',
  providers: [
    {
      provide: TUI_BUTTON_OPTIONS,
      useValue: {
        appearance: TuiAppearance.Primary,
        shape: 'rounded'
      },
    },
  ]
})
export class ButtonPrimaryComponent {
  @Input() disabled: boolean = false;
  @Input() size: TuiSizeXS | TuiSizeXL = 'm';
  @Input() showLoader: boolean = false;
  @Input() icon: any;
  @Input() iconRight: any;
  @Input() focusable: boolean = false;
}
