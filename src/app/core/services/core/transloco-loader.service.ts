import {inject, Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Translation, TranslocoLoader} from "@ngneat/transloco";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class TranslocoLoaderService implements TranslocoLoader{

  private httpClient: HttpClient = inject<HttpClient>(HttpClient);

  getTranslation(lang: string): Observable<Translation> {
    return this.httpClient.get<Translation>(`./assets/i18n/${lang}.json`);
  }
}
