import {Directive, ElementRef, inject} from '@angular/core';

@Directive({
  selector: '[appMHeaderLeft]'
})
export class MHeaderLeftDirective {

  public el: ElementRef = inject<ElementRef>(ElementRef);

}
