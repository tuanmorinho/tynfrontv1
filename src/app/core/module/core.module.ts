import {DEFAULT_CURRENCY_CODE, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseComponent } from './base/base.component';
import {AppCoreProviderFactory} from "@core/DI/init-factory";
import {TranslocoCoreModule} from "@core/module/transloco/transloco.module";
import {TranslocoModule} from "@ngneat/transloco";
import {HttpClientModule} from "@angular/common/http";
import {DirectiveModule} from "@core/module/directive/directive.module";
import {PipeModule} from "@core/module/pipe/pipe.module";



@NgModule({
  declarations: [
    BaseComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    TranslocoCoreModule
  ],
  exports: [
    BaseComponent,
    TranslocoModule,
    PipeModule,
    DirectiveModule
  ],
  providers: [
    AppCoreProviderFactory,
    {provide: DEFAULT_CURRENCY_CODE, useValue: 'VND'},
  ]
})
export class CoreModule { }
