import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChild, ElementRef,
  inject,
  Input, OnInit, Renderer2,
  ViewChild
} from '@angular/core';
import {BaseComponent} from "@core/module/base/base.component";
import {MHeaderLeftDirective} from "@core/module/directive/m-header-left.directive";
import {MHeaderRightDirective} from "@core/module/directive/m-header-right.directive";
import {MHeaderCenterDirective} from "@core/module/directive/m-header-center.directive";
import {Observable} from "rxjs";
import {MHeaderConfigState} from "@core/store/m-header-config/m-header-config.state";
import {select} from "@ngrx/store";
import {selectMHeaderConfig} from "@core/store/m-header-config/m-header-config.selector";

@Component({
  selector: 'app-m-header',
  templateUrl: './m-header.component.html',
  styleUrls: ['./m-header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MHeaderComponent extends BaseComponent implements OnInit, AfterViewInit {

  rightHeaderLength: number = 0;
  totalCartQty: number = 2;

  private cdr: ChangeDetectorRef = inject<ChangeDetectorRef>(ChangeDetectorRef);
  private renderer: Renderer2 = inject<Renderer2>(Renderer2);

  @ContentChild(MHeaderLeftDirective, {static: false}) mHeaderLeftContent!: MHeaderLeftDirective;
  @ContentChild(MHeaderRightDirective, {static: false}) mHeaderRightContent!: MHeaderRightDirective;
  @ContentChild(MHeaderCenterDirective, {static: false}) mHeaderCenterContent!: MHeaderCenterDirective;

  @ViewChild('rightHeaderEL', {static: false}) rightHeader!: ElementRef;

  mHeaderConfig$: Observable<MHeaderConfigState | undefined> = this.store.pipe(select(selectMHeaderConfig), this.onDestroy<MHeaderConfigState | undefined>());

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    this.updateGridStyle();
  }

  mBack() {}

  mToCart() {}

  private updateGridStyle() {
    if (this.rightHeader && this.rightHeader?.nativeElement) {
      this.renderer.setStyle(this.rightHeader.nativeElement, 'display', 'grid');
      this.renderer.setStyle(this.rightHeader.nativeElement, 'grid-template-columns', `repeat(${this.rightHeader.nativeElement?.children?.length}, minmax(0, 1fr))`);
      this.cdr.detectChanges();
    }
  }
}
