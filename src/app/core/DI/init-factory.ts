import {APP_INITIALIZER, Provider} from "@angular/core";
import {initializeDeviceFactory, initializeVisitorUuidFactory} from "./app-init";
import {Store} from "@ngrx/store";
import {AppState} from "@core/store/app.state";
import {CookieService} from "@core/services/core";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {AuthInterceptor} from "@core/interceptor/auth.interceptor";

export const AppCoreProviderFactory: Provider[] = [
    {
        provide: APP_INITIALIZER,
        useFactory: initializeDeviceFactory,
        deps: [Store<AppState>],
        multi: true
    },
    {
        provide: APP_INITIALIZER,
        useFactory: initializeVisitorUuidFactory,
        deps: [CookieService],
        multi: true
    },
    {
        provide: HTTP_INTERCEPTORS,
        useClass: AuthInterceptor,
        multi: true
    }
]
