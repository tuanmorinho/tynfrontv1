import {Component, Input} from '@angular/core';
import {BaseComponent} from "@core/module/base/base.component";
import {ISurvey} from "@core/models/features";
import {environment} from "../../../../enviroments";
import {ROUTING_CONSTANT, ROUTING_TYN} from "@core/constants/routing-constant";

@Component({
    selector: 'app-survey-card-item',
    templateUrl: './survey-card-item.component.html'
})
export class SurveyCardItemComponent extends BaseComponent {
    @Input() survey!: ISurvey;

    urlImageBase: string = environment.apiUrl.base.replace('/v2', '');

    navigateToSurveyDetail(): void {
        if (this.survey && this.survey.id) {
            void this.router.navigate([`${ROUTING_TYN.SURVEY}/${ROUTING_CONSTANT.DETAIL}/${this.survey.id}`])
        }
    }
}
