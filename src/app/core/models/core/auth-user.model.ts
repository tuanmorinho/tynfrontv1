import { IAccountModel } from "./account.model";

export interface IAuthUser {
  roles: string[],
  sub: string;
  token: string;
  account: IAccountModel;
}
