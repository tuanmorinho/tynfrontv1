import {inject, Injectable} from '@angular/core';
import {environment} from "../../../../enviroments";
import {HttpClient} from "@angular/common/http";
import {IRecommendRequest, IRecommendResponse, ISurvey} from "@core/models/features";
import {Observable} from "rxjs";

@Injectable({
    providedIn: 'root'
})
export class RecommendService {

    API_URL: string = environment.apiUrl.base + '/recommend';

    private http: HttpClient = inject<HttpClient>(HttpClient);

    getResult(recommendRequest: IRecommendRequest): Observable<IRecommendResponse> {
        return this.http.post<IRecommendResponse>(this.API_URL + '/getResult', recommendRequest);
    }
}
