import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {SurveyListPageComponent} from '@pages/survey-page/survey-list-page/survey-list-page.component';
import {SurveyDetailPageComponent} from '@pages/survey-page/survey-detail-page/survey-detail-page.component';
import {ROUTING_CONSTANT} from "@core/constants/routing-constant";
import {CoreModule} from "@core/module/core.module";
import {SharedModule} from "@shared/shared.module";
import { QuizPageComponent } from './quiz-page/quiz-page.component';
import {TuiSvgModule} from "@taiga-ui/core";
import {TuiMarkerIconModule, TuiSliderModule} from "@taiga-ui/kit";
import {ReactiveFormsModule} from "@angular/forms";

const routes: Routes = [
    {
        path: ROUTING_CONSTANT.LIST + '/:id',
        component: SurveyListPageComponent,
        data: {wShowFooter: true}
    },
    {
        path: ROUTING_CONSTANT.DETAIL + '/:id',
        component: SurveyDetailPageComponent,
        data: {wShowFooter: false, wHideNav: true}
    },
    {
        path: ROUTING_CONSTANT.QUIZ + '/:id',
        component: QuizPageComponent,
        data: {wShowFooter: false, wHideNav: true}
    }
]

@NgModule({
    declarations: [
        SurveyListPageComponent,
        SurveyDetailPageComponent,
        QuizPageComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        CoreModule,
        SharedModule,
        TuiSvgModule,
        TuiMarkerIconModule,
        TuiSliderModule,
        ReactiveFormsModule
    ]
})
export class SurveyPageModule {
}
