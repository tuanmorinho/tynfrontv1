import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MobilePageComponent} from './mobile-page.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "@shared/shared.module";
import {CoreModule} from "@core/module/core.module";

const routes: Routes = [
  {
    path: '',
    component: MobilePageComponent
  }
]

@NgModule({
    declarations: [
        MobilePageComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedModule,
        CoreModule
    ]
})
export class MobilePageModule {
}
