export interface MHeaderConfigState {
  title?: string | null;
  showMenu?: boolean;
  showBack?: boolean;
  showSearch?: boolean;
  showSetting?: boolean;
  showCart?: boolean;
}
