import {isDevMode, LOCALE_ID, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {TuiRootModule} from '@taiga-ui/core';
import {LayoutModule} from "@layout/layout.module";
import {CoreModule} from "@core/module/core.module";
import {SharedModule} from "@shared/shared.module";
import {StoreDevtoolsModule} from "@ngrx/store-devtools";
import {rootReducer} from "@core/store/rootReducer";
import {StoreModule} from "@ngrx/store";
import {APP_BASE_HREF} from "@angular/common";
import {ToastrModule} from "ngx-toastr";

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        TuiRootModule,
        AppRoutingModule,
        LayoutModule,
        CoreModule,
        SharedModule,
        StoreModule.forRoot(rootReducer),
        StoreDevtoolsModule.instrument({
            maxAge: 25,
            logOnly: !isDevMode(),
            autoPause: true,
            trace: true,
            traceLimit: 75,
        }),
        ToastrModule.forRoot()
    ],
    providers: [
        {
        provide: APP_BASE_HREF,
        useValue: '/'
        },
        {
            provide: LOCALE_ID,
            useValue: 'en-US'
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
