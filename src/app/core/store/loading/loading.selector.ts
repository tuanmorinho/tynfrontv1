import {AppState} from "../app.state";

export const loading = (state: AppState) => state.feature_loading;
export const skeleton = (state: AppState) => state.feature_loading.skeletonVisible;

