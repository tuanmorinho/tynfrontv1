import {createAction, props} from "@ngrx/store";

export const SetDeviceType: string = '@Device/SetType';
export const SetDeviceMobile: string = '@Device/SetMobile';
export const SetDeviceDesktop: string = '@Device/SetDesktop'

export const setDeviceType = createAction(SetDeviceType, props<{payload: string}>());
export const setDeviceMobile = createAction(SetDeviceMobile, props<{payload: string}>());
export const setDeviceDesktop = createAction(SetDeviceDesktop, props<{payload: string}>());
