import {DeviceState} from "./device/device.state";
import {LayoutState} from "./layout/layout.state";
import {SearchState} from "./search/search.state";
import {MHeaderConfigState} from "./m-header-config/m-header-config.state";
import {LoadingState} from "@core/store/loading/loading.state";
import {AuthState} from "@core/store/auth/auth.state";

export interface AppState {
  feature_device: DeviceState,
  feature_layout: LayoutState,
  feature_search: SearchState,
  feature_mHeaderConfig: MHeaderConfigState
  feature_loading: LoadingState,
  feature_auth: AuthState
}
