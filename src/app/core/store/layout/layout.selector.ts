import {AppState} from "../app.state";
import {createSelector} from "@ngrx/store";
import {LayoutState, selectLayoutOption} from "./layout.state";

export const selectLayout = (state: AppState) => state.feature_layout;

export const selectLayoutEl = (property: keyof typeof selectLayoutOption) => createSelector(
  selectLayout,
  (layoutState: LayoutState) => layoutState[property]
)

export const selectLayoutEls = (properties: Array<keyof typeof selectLayoutOption>) => createSelector(
  selectLayout,
  (layoutState: LayoutState) => {
    return properties.map(property => layoutState[property]);
  }
)
