export interface ISurveyAccountAnswer {
    surveyAnswerId?: number;
    answerText?: string;
    answerDemandScore?: number;
    answerImageUpload1?: string;
    answerImageUpload2?: string;
    answerImageUpload3?: string;
    answerImageUpload4?: string;
    answerImageUpload5?: string;
    answerDisplayType?: number;
}