import {Component, inject, OnInit} from '@angular/core';
import {BaseComponent} from "@core/module/base/base.component";
import {ActivatedRoute} from "@angular/router";
import {RecommendService} from "@core/services/features";
import {IProduct, IRecommendResponse} from "@core/models/features";
import {delay} from "rxjs";

@Component({
    selector: 'app-product-recommend-page',
    templateUrl: './product-recommend-page.component.html'
})
export class ProductRecommendPageComponent extends BaseComponent implements OnInit {
    surveyId: number | undefined;
    productResult: IProduct | undefined;

    private route: ActivatedRoute = inject<ActivatedRoute>(ActivatedRoute);
    private recommendService: RecommendService = inject<RecommendService>(RecommendService);

    ngOnInit(): void {
        if (this.route.snapshot.params['id']) {
            this.surveyId = +this.route.snapshot.params['id'];
            this.getProductRecommend();
        }
    }

    private getProductRecommend(): void {
        if (this.surveyId) {
            this.isLoading = true;
            this.recommendService.getResult({surveyId: this.surveyId})
                .pipe(
                    delay(500)
                )
                .subscribe({
                    next: (data: IRecommendResponse): void => {
                        if (data && data.product) {
                            this.productResult = data.product;
                        }
                        this.isLoading = false
                    },
                    error: (): void => {
                        this.isLoading = false;
                        void this.router.navigateByUrl('error');
                    }
                })
        }
    }
}
