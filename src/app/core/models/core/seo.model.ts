import {ValueOf} from "@core/models/core/base.model";

export interface SeoMeta {
  title?: string;
  description?: string;
}

export interface RequestSeoMeta {
  objectId?: number;
  type?: ValueOf<typeof SEO_TYPE_CONSTANT>;
  keywords?: string;
}

export const SEO_TYPE_CONSTANT = {
  MOVIE: 1,
  SHOW_TIME: 2,
  STORE: 3,
  LANDING: 4,
  PROMOTION: 5
}
