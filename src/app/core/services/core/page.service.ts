import {inject, Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {RequestSeoMeta, SeoMeta} from "@core/models/core";
import {environment} from "../../../../enviroments";

@Injectable({
  providedIn: 'root'
})
export class PageService {

  private url: string = environment.apiUrl.base + '/v2';

  private http: HttpClient = inject<HttpClient>(HttpClient);

  getSeoMetaConfig(body?: RequestSeoMeta): Observable<SeoMeta> {
    return this.http.post<SeoMeta>(this.url + '/meta/getMetaConfig', body || {});
  }
}
