import {deviceReducer} from "./device/device.reducer";
import {layoutReducer} from "./layout/layout.reducer";
import {searchReducer} from "./search/search.reducer";
import {mHeaderConfigReducer} from "./m-header-config/m-header-config.reducer";
import {loadingReducer} from "@core/store/loading/loading.reducer";
import {authReducer} from "@core/store/auth/auth.reducer";

export const rootReducer = {
  feature_device: deviceReducer,
  feature_layout: layoutReducer,
  feature_search: searchReducer,
  feature_mHeaderConfig: mHeaderConfigReducer,
  feature_loading: loadingReducer,
  feature_auth: authReducer
}
