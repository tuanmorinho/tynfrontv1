import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SurveyCategoryListComponent} from './survey-category-list/survey-category-list.component';
import {SurveyCategoryCardItemComponent} from './survey-category-card-item/survey-category-card-item.component';
import {CoreModule} from "@core/module/core.module";
import {IconModule} from "@shared/icon/icon.module";
import {TuiSvgModule} from "@taiga-ui/core";


@NgModule({
    declarations: [
        SurveyCategoryListComponent,
        SurveyCategoryCardItemComponent
    ],
    imports: [
        CommonModule,
        CoreModule,
        IconModule,
        TuiSvgModule
    ],
    exports: [
        SurveyCategoryListComponent,
        SurveyCategoryCardItemComponent
    ]
})
export class SurveyCategoryModule {
}
