import {createReducer, on} from "@ngrx/store";
import {LayoutState} from "./layout.state";
import {setLayout, updateLayout} from "./layout.actions";

const initialState: LayoutState = {
    wHideNav: false,
    wShowFooter: true
};

export const layoutReducer = createReducer(
  initialState,
  on(setLayout, (state: LayoutState, {payload}) => payload),
  on(updateLayout, (state: LayoutState, {payload}) => ({...state, ...payload}))
);
