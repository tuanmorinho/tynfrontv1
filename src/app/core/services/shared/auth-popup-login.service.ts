import {
  ApplicationRef,
  ComponentRef,
  inject,
  Injectable,
  Injector,
  NgModuleRef,
  Compiler,
  Renderer2
} from '@angular/core';
import {AuthPopupLoginComponent} from "@shared/auth-form/auth-popup-login/auth-popup-login.component";
import {AuthFormModule} from "@shared/auth-form/auth-form.module";
import {ComponentPortal, DomPortalOutlet} from "@angular/cdk/portal";
import {DOCUMENT} from "@angular/common";
import {NavigationExtras} from "@angular/router";
import {AUTH_FORM_TYPE} from "@core/constants/auth-form";
import {Observable, of} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class AuthPopupLoginService {

  private authModuleRef!: NgModuleRef<AuthFormModule>;
  private authPopupLoginComponentRef!: ComponentRef<AuthPopupLoginComponent>;
  private portalOutlet!: DomPortalOutlet;
  private portal!: ComponentPortal<AuthPopupLoginComponent>;

  private document: Document = inject<Document>(DOCUMENT);
  private appRef: ApplicationRef = inject<ApplicationRef>(ApplicationRef);
  private injector: Injector = inject<Injector>(Injector);
  private renderer2: Renderer2 = inject<Renderer2>(Renderer2);

  private requestAuth(isLoginSMS?: boolean): Observable<boolean> {
    return this.requestAuthObsevable(isLoginSMS);
  }

  private requestAuthObsevable(isLoginSMS?: boolean, formType?: typeof AUTH_FORM_TYPE, extras?: NavigationExtras): Observable<boolean> {
    if (true) return of(true);
    if (false) {
      this.openLoginForm(isLoginSMS, formType, extras);
      return of (false);
    }
  }

  private openLoginForm(isLoginSMS?: boolean, formType?: typeof AUTH_FORM_TYPE, extras?: NavigationExtras) {

  }
}
