export interface BaseModel {
  id?: number;
}

export interface BasePaginationRequest {
  page?: number,
  take?: number,
  sort?: string,
}

export type ValueOf<T> = T[keyof T];
