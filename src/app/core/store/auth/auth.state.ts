import {IAccountModel} from "@core/models/core";

export interface AuthState {
    account: IAccountModel | null;
}