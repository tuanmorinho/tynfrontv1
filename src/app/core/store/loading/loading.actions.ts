import {createAction, props} from "@ngrx/store";
import {LoadingState} from "@core/store/loading/loading.state";

export const SetLoading: string = '@Loading/SetLoading';
export const SetSkeleton: string = '@Loading/SetSkeleton';

export const setLoading = createAction(SetLoading, props<{ payload: LoadingState }>());
export const setSkeleton = createAction(SetSkeleton, props<{ payload: boolean }>());
