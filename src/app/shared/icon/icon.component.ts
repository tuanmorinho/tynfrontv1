import {
  ChangeDetectionStrategy,
  Component,
  HostBinding,
  inject,
  Input,
  PLATFORM_ID
} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import {isPlatformBrowser} from '@angular/common';
import {svgIcons, IconTypes} from "./icon";
import {ISvgIcon} from "../../core/models/shared";

@Component({
  selector: 'app-icon',
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IconComponent {
  icons: typeof svgIcons = svgIcons;
  icon: ISvgIcon | null = null;
  isPlatformBrowser: boolean = false;

  @Input() set type(val: IconTypes) {
    this.icon = {
      ...this.icons[val],
      svgInner: this.sanitizer.bypassSecurityTrustHtml(svgIcons[val].svgInner as string)
    }
  };

  @HostBinding('attr.aria-hidden') attrHidden: boolean = true;
  @HostBinding('attr.role') role: string = 'img';

  private sanitizer: DomSanitizer = inject<DomSanitizer>(DomSanitizer);
  private  platformId: Object = inject<Object>(PLATFORM_ID);

  constructor() {
    this.isPlatformBrowser = isPlatformBrowser(this.platformId);
  }
}
