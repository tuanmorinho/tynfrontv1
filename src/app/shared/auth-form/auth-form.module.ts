import {ComponentRef, inject, NgModule, ViewContainerRef} from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthPopupLoginComponent } from './auth-popup-login/auth-popup-login.component';



@NgModule({
  declarations: [
    AuthPopupLoginComponent
  ],
  imports: [
    CommonModule
  ]
})
export class AuthFormModule {
  private viewContainerRef: ViewContainerRef = inject<ViewContainerRef>(ViewContainerRef);

  public createAuthPopupLoginComponent(): ComponentRef<AuthPopupLoginComponent> {
    return this.viewContainerRef.createComponent(AuthPopupLoginComponent);
  }
}
