export interface LayoutState {
  mHideNav?: boolean;
  mShowFooter?: boolean;
  wHideNav?: boolean;
  wShowFooter?: boolean;
}

export type tLayoutOptions = {
  [key: string]: boolean
}

export const selectLayoutOption = {
  mHideNav: false,
  mShowFooter: true,
  wHideNav: false,
  wShowFooter: true
}
