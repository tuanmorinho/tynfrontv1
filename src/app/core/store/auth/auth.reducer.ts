import {createReducer, on} from "@ngrx/store";
import {AuthState} from "@core/store/auth/auth.state";
import {setAuthUser} from "@core/store/auth/auth.actions";

const initialState: AuthState = {
    account: null
};

export const authReducer = createReducer(
    initialState,
    on(setAuthUser, (state: AuthState, {payload}) => payload),
);
