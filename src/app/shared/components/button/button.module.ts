import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonPrimaryComponent } from './button-primary/button-primary.component';
import {TuiButtonModule} from "@taiga-ui/core";


@NgModule({
  declarations: [
    ButtonPrimaryComponent
  ],
  imports: [
    CommonModule,
    TuiButtonModule
  ],
  providers: [],
  exports: [
    ButtonPrimaryComponent
  ]
})
export class ButtonModule { }
