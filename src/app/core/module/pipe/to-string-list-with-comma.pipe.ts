import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'toStringListWithComma'
})
export class ToStringListWithCommaPipe implements PipeTransform {

  transform(list: any[] | undefined, prop: string, props?: string[]): string {
    let listWithComma: string = '';
    if (prop && !props) {
      list && list.length > 0 ? list?.forEach((v: any, index: number) => {
        if (index == list.length - 1) {
          listWithComma += v[prop];
        } else {
          listWithComma += `${v[prop]}, `
        }
      }) : '';
    }
    if (!prop && props && props?.length > 0) {
      list && list.length > 0 ? list?.forEach((v: any, index: number) => {
        if (index == list.length - 1) {
          listWithComma += this.concatProps(v, props);
        } else {
          listWithComma += `${this.concatProps(v, props)}, `;
        }
      }) : '';
    }
    return listWithComma;
  }

  private concatProps(v: any, props: string[]): string {
    let concat: string = '';
    props.forEach((prop, index) => {
      if (index == props.length - 1) {
        concat += v[prop];
      } else {
        concat += `${v[prop]} `;
      }
    })
    return concat;
  }

}
