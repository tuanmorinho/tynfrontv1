import {Observable, of} from "rxjs";
import {inject} from "@angular/core";
import {switchMap} from "rxjs/operators";
import {select, Store} from "@ngrx/store";
import {AppState} from "@core/store/app.state";
import {selectDevice} from "@core/store/device/device.selector";
import {DeviceState} from "@core/store/device/device.state";
import {Router} from "@angular/router";

export const mobilePageGuard = (): Observable<boolean> => {
    const router: Router = inject<Router>(Router);
    const store: Store<AppState> = inject(Store<AppState>);

    return store.pipe(select(selectDevice)).pipe(
        switchMap((device: DeviceState): Observable<boolean> => {
            if (device.isMobile) return of(true);
            void router.navigateByUrl('/');
            return of(false);
        })
    )
}