import {AppState} from "@core/store/app.state";

export const selectMHeaderConfig = (state: AppState) => state.feature_mHeaderConfig;
