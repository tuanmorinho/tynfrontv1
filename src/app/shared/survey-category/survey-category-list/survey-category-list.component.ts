import {Component, inject, OnInit} from '@angular/core';
import {BaseComponent} from "@core/module/base/base.component";
import {SurveyCategoryService} from "@core/services/features";
import {ISurveyCategory} from "@core/models/features";

@Component({
    selector: 'app-survey-category-list',
    templateUrl: './survey-category-list.component.html'
})
export class SurveyCategoryListComponent extends BaseComponent implements OnInit {

    surveyCategoryList: ISurveyCategory[] = [];

    private surveyCategoryService: SurveyCategoryService = inject<SurveyCategoryService>(SurveyCategoryService);

    ngOnInit(): void {
        this.findAllSurveyCategory();
    }

    private findAllSurveyCategory(): void {
        this.isLoading = true;
        this.surveyCategoryService.findAll().subscribe({
            next: (data: ISurveyCategory[]): void => {
                this.surveyCategoryList = data;
                this.isLoading = false;
            }
        })
    }
}
