import {ChangeDetectionStrategy, Component} from '@angular/core';
import {RoutingContants} from "@core/models/core";
import {ROUTING_TYN} from "@core/constants/routing-constant";

@Component({
  selector: 'app-m-navigation',
  templateUrl: './m-navigation.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MNavigationComponent {
  protected readonly ROUTING_TYN: RoutingContants = ROUTING_TYN;

  toNotification() {}
}
