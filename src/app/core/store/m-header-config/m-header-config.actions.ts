import {createAction, props} from "@ngrx/store";
import {MHeaderConfigState} from "./m-header-config.state";

export const SetMHeaderConfig: string = '@MHeaderConfig/SetMHeaderConfig';
export const UpdateMHeaderConfig: string = '@MHeaderConfig/UpdateMHeaderConfig';

export const setMHeaderConfig = createAction(SetMHeaderConfig, props<{payload: MHeaderConfigState}>());
export const updateMHeaderConfig = createAction(UpdateMHeaderConfig, props<{payload: Partial<MHeaderConfigState>}>());
