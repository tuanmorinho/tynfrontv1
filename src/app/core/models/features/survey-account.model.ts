export interface ISurveyAccount {
    id?: number;
    surveyId?: number;
    accountId?: number;
    visitorUuid?: string;
    actionStatus?: number;
    actionDate?: Date;
}