import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomePageComponent } from './home-page.component';
import {RouterModule, Routes} from "@angular/router";
import {CoreModule} from "@core/module/core.module";
import {SharedModule} from "@shared/shared.module";

const routes: Routes = [
  {
    path: '',
    component: HomePageComponent,
    data: { wShowFooter: true }
  }
]


@NgModule({
  declarations: [
    HomePageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    CoreModule,
    SharedModule
  ]
})
export class HomePageModule { }
