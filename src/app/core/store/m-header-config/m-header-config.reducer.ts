import {createReducer, on} from "@ngrx/store";
import {MHeaderConfigState} from "./m-header-config.state";
import {setMHeaderConfig, updateMHeaderConfig} from "./m-header-config.actions";

const initialState: MHeaderConfigState = {
  title: null,
  showBack: true,
  showMenu: false,
  showSearch: false,
  showSetting: false,
  showCart: false
};

export const mHeaderConfigReducer = createReducer(
  initialState,
  on(setMHeaderConfig, (state: MHeaderConfigState, {payload}) => payload),
  on(updateMHeaderConfig, (state: MHeaderConfigState, {payload}) => ({...state, ...payload}))
);
