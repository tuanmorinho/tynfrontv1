import {ISvgIcon} from "../../core/models/shared";

export const icBarcodeError: ISvgIcon = {
  viewBox: '0 0 20 20',
  svgInner: '<g xmlns="http://www.w3.org/2000/svg" transform="translate(0.000000,704.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none">\n' +
    '<path d="M700 4010 l0 -2950 70 0 70 0 0 2950 0 2950 -70 0 -70 0 0 -2950z"/>\n' +
    '<path d="M940 4010 l0 -2950 30 0 30 0 0 2950 0 2950 -30 0 -30 0 0 -2950z"/>\n' +
    '<path d="M1170 4010 l0 -2950 30 0 30 0 0 2950 0 2950 -30 0 -30 0 0 -2950z"/>\n' +
    '<path d="M1570 4010 l0 -2950 145 0 145 0 0 2950 0 2950 -145 0 -145 0 0 -2950z"/>\n' +
    '<path d="M1960 4010 l0 -2950 30 0 30 0 0 2950 0 2950 -30 0 -30 0 0 -2950z"/>\n' +
    '<path d="M2120 4010 l0 -2950 110 0 110 0 0 2950 0 2950 -110 0 -110 0 0 -2950z"/>\n' +
    '<path d="M2440 4010 l0 -2950 105 0 105 0 0 2950 0 2950 -105 0 -105 0 0 -2950z"/>\n' +
    '<path d="M2750 4010 l0 -2950 30 0 30 0 0 2950 0 2950 -30 0 -30 0 0 -2950z"/>\n' +
    '<path d="M3060 4010 l0 -2950 70 0 70 0 0 2950 0 2950 -70 0 -70 0 0 -2950z"/>\n' +
    '<path d="M3300 4010 l0 -2950 30 0 30 0 0 2950 0 2950 -30 0 -30 0 0 -2950z"/>\n' +
    '<path d="M3690 4010 l0 -2950 70 0 70 0 0 2950 0 2950 -70 0 -70 0 0 -2950z"/>\n' +
    '<path d="M3930 4010 l0 -2950 30 0 30 0 0 2950 0 2950 -30 0 -30 0 0 -2950z"/>\n' +
    '<path d="M4170 4010 l0 -2950 65 0 65 0 0 2950 0 2950 -65 0 -65 0 0 -2950z"/>\n' +
    '<path d="M4640 4010 l0 -2950 30 0 30 0 0 2950 0 2950 -30 0 -30 0 0 -2950z"/>\n' +
    '<path d="M4880 4010 l0 -2950 30 0 30 0 0 2950 0 2950 -30 0 -30 0 0 -2950z"/>\n' +
    '<path d="M5040 4010 l0 -2950 25 0 25 0 0 2950 0 2950 -25 0 -25 0 0 -2950z"/>\n' +
    '<path d="M5430 4010 l0 -2950 70 0 70 0 0 2950 0 2950 -70 0 -70 0 0 -2950z"/>\n' +
    '<path d="M5660 4010 l0 -2950 30 0 30 0 0 2950 0 2950 -30 0 -30 0 0 -2950z"/>\n' +
    '<path d="M5900 4010 l0 -2950 30 0 30 0 0 2950 0 2950 -30 0 -30 0 0 -2950z"/>\n' +
    '<path d="M6060 4010 l0 -2950 30 0 30 0 0 2950 0 2950 -30 0 -30 0 0 -2950z"/>\n' +
    '<path d="M6290 4010 l0 -2950 150 0 150 0 0 2950 0 2950 -150 0 -150 0 0 -2950z"/>\n' +
    '<path d="M6770 4010 l0 -2950 30 0 30 0 0 2950 0 2950 -30 0 -30 0 0 -2950z"/>\n' +
    '<path d="M6930 4010 l0 -2950 65 0 65 0 0 2950 0 2950 -65 0 -65 0 0 -2950z"/>\n' +
    '<path d="M7240 4010 l0 -2950 30 0 30 0 0 2950 0 2950 -30 0 -30 0 0 -2950z"/>\n' +
    '<path d="M7630 4010 l0 -2950 30 0 30 0 0 2950 0 2950 -30 0 -30 0 0 -2950z"/>\n' +
    '<path d="M8020 4010 l0 -2950 30 0 30 0 0 2950 0 2950 -30 0 -30 0 0 -2950z"/>\n' +
    '<path d="M8260 4010 l0 -2950 70 0 70 0 0 2950 0 2950 -70 0 -70 0 0 -2950z"/>\n' +
    '<path d="M8500 4010 l0 -2950 30 0 30 0 0 2950 0 2950 -30 0 -30 0 0 -2950z"/>\n' +
    '<path d="M8890 4010 l0 -2950 70 0 70 0 0 2950 0 2950 -70 0 -70 0 0 -2950z"/>\n' +
    '<path d="M9130 4010 l0 -2950 30 0 30 0 0 2950 0 2950 -30 0 -30 0 0 -2950z"/>\n' +
    '<path d="M9365 4010 l0 -2950 28 0 27 0 0 2950 0 2950 -27 0 -28 0 0 -2950z"/>\n' +
    '<path d="M9600 4010 l0 -2950 30 0 30 0 0 2950 0 2950 -30 0 -30 0 0 -2950z"/>\n' +
    '<path d="M9760 4010 l0 -2950 70 0 70 0 0 2950 0 2950 -70 0 -70 0 0 -2950z"/>\n' +
    '<path d="M10230 4010 l0 -2950 30 0 30 0 0 2950 0 2950 -30 0 -30 0 0 -2950z"/>\n' +
    '<path d="M10390 4010 l0 -2950 145 0 145 0 0 2950 0 2950 -145 0 -145 0 0 -2950z"/>\n' +
    '<path d="M10780 4010 l0 -2950 110 0 110 0 0 2950 0 2950 -110 0 -110 0 0 -2950z"/>\n' +
    '<path d="M11100 4010 l0 -2950 70 0 70 0 0 2950 0 2950 -70 0 -70 0 0 -2950z"/>\n' +
    '<path d="M11490 4010 l0 -2950 110 0 110 0 0 2950 0 2950 -110 0 -110 0 0 -2950z"/>\n' +
    '<path d="M11800 4010 l0 -2950 30 0 30 0 0 2950 0 2950 -30 0 -30 0 0 -2950z"/>\n' +
    '<path d="M11960 4010 l0 -2950 70 0 70 0 0 2950 0 2950 -70 0 -70 0 0 -2950z"/>\n' +
    '</g>'
}

export const icHome: ISvgIcon = {
  viewBox: '0 0 24 24',
  svgInner: '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>'
}

export const icFilm: ISvgIcon = {
  viewBox: '0 0 24 24',
  svgInner: '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-film"><rect x="2" y="2" width="20" height="20" rx="2.18" ry="2.18"></rect><line x1="7" y1="2" x2="7" y2="22"></line><line x1="17" y1="2" x2="17" y2="22"></line><line x1="2" y1="12" x2="22" y2="12"></line><line x1="2" y1="7" x2="7" y2="7"></line><line x1="2" y1="17" x2="7" y2="17"></line><line x1="17" y1="17" x2="22" y2="17"></line><line x1="17" y1="7" x2="22" y2="7"></line></svg>'
}

export const icCart: ISvgIcon = {
  viewBox: '0 0 24 24',
  svgInner: '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-shopping-cart"><circle cx="9" cy="21" r="1"></circle><circle cx="20" cy="21" r="1"></circle><path d="M1 1h4l2.68 13.39a2 2 0 0 0 2 1.61h9.72a2 2 0 0 0 2-1.61L23 6H6"></path></svg>'
}

export const icNotification: ISvgIcon = {
  viewBox: '0 0 24 24',
  svgInner: '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bell"><path d="M18 8A6 6 0 0 0 6 8c0 7-3 9-3 9h18s-3-2-3-9"/><path d="M13.73 21a2 2 0 0 1-3.46 0"/></svg>'
}

export const icAccount: ISvgIcon = {
  viewBox: '0 0 24 24',
  svgInner: '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>'
}

export const icShop: ISvgIcon = {
  viewBox: '0 0 24 24',
  svgInner: '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-shopping-bag"><path d="M6 2L3 6v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2V6l-3-4z"></path><line x1="3" y1="6" x2="21" y2="6"></line><path d="M16 10a4 4 0 0 1-8 0"></path></svg>'
}

export const icMenuDot: ISvgIcon = {
  viewBox: '0 0 24 24',
  svgInner: '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-more-vertical"><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg>'
}

export const icMHeaderBack: ISvgIcon = {
  viewBox: '0 0 24 24',
  svgInner: '<svg xmlns="http://www.w3.org/2000/svg" width="60" height="35" viewBox="0 0 60 35" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>'
}

export const icMHeaderSetting: ISvgIcon = {
  viewBox: '0 0 24 24',
  svgInner: '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-settings"><circle cx="12" cy="12" r="3"></circle><path d="M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z"></path></svg>'
}

export const icMovieTime: ISvgIcon = {
  viewBox: '0 0 24 24',
  svgInner: '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-clock"><circle cx="12" cy="12" r="10"></circle><polyline points="12 6 12 12 16 14"></polyline></svg>'
}

export const svgIcons = {
  icBarcodeError,
  icHome,
  icFilm,
  icCart,
  icNotification,
  icAccount,
  icShop,
  icMenuDot,
  icMHeaderBack,
  icMHeaderSetting,
  icMovieTime
}

export type IconTypes = keyof typeof svgIcons;
