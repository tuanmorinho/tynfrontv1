import {DeviceState} from "./device.state";
import {setDeviceDesktop, setDeviceMobile, setDeviceType} from "./device.actions";
import {createReducer, on} from "@ngrx/store";

const initialState: DeviceState = {
  deviceType: '',
  isMobile: false,
  isDesktop: false
};

export const deviceReducer = createReducer(
  initialState,
  on(setDeviceType, (state: DeviceState, {payload}) => ({ ...state, deviceType: payload })),
  on(setDeviceMobile, (state: DeviceState, {payload}) => ({...state, isMobile: payload === 'mobile'})),
  on(setDeviceDesktop, (state: DeviceState, {payload}) => ({...state, isDesktop: payload !== 'mobile'}))
);
