import {Component, inject, OnInit} from '@angular/core';
import {BaseComponent} from "@core/module/base/base.component";
import {
  ActivatedRoute,
  Data,
  Event,
  NavigationEnd,
  RouteConfigLoadEnd,
  RouteConfigLoadStart,
  Scroll
} from "@angular/router";
import {setLayout} from "@core/store/layout/layout.actions";
import {Observable} from "rxjs";
import {selectLayoutEl} from "@core/store/layout/layout.selector";
import {select} from "@ngrx/store";

@Component({
    selector: 'app-main',
    templateUrl: './main.component.html',
})
export class MainComponent extends BaseComponent implements OnInit {
    private route: ActivatedRoute = inject<ActivatedRoute>(ActivatedRoute);

    mHideNav$: Observable<boolean | undefined> = this.store.pipe(select(selectLayoutEl('mHideNav')), this.onDestroy<boolean | undefined>());
    mShowFooter$: Observable<boolean | undefined> = this.store.pipe(select(selectLayoutEl('mShowFooter')), this.onDestroy<boolean | undefined>());
    wHideNav$: Observable<boolean | undefined> = this.store.pipe(select(selectLayoutEl('wHideNav')), this.onDestroy<boolean | undefined>());
    wShowFooter$: Observable<boolean | undefined> = this.store.pipe(select(selectLayoutEl('wShowFooter')), this.onDestroy<boolean | undefined>());

    protected readonly Boolean: BooleanConstructor = Boolean;

    ngOnInit(): void {
        this.watchRouterEvent();
    }

    private watchRouterEvent(): void {
        this.router.events.forEach((event: Event): void => {
            // if (event instanceof RouteConfigLoadStart) this.isLoading = true;
            // if (event instanceof RouteConfigLoadEnd) this.isLoading = false;
            if (event instanceof NavigationEnd || event instanceof Scroll) {
                // this.isLoading = false;
                const baseRouteData: Data | undefined = this.route.firstChild?.firstChild?.snapshot.data;
                if (baseRouteData) {
                    this.store.dispatch(setLayout({
                        payload: {
                            mHideNav: baseRouteData['mHideNav'],
                            mShowFooter: baseRouteData['mShowFooter'],
                            wHideNav: baseRouteData['wHideNav'],
                            wShowFooter: baseRouteData['wShowFooter']
                        }
                    }));
                }
            }
        })
    }
}
