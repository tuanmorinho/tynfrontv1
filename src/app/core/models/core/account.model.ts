export interface IAccountModel {
    fullName?: string;
    avatarUrl?: string;
    email?: string;
    mobile?: string;
}
