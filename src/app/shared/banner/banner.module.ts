import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BannerComponent } from './banner.component';
import {TuiCarouselModule} from "@taiga-ui/kit";



@NgModule({
  declarations: [
    BannerComponent
  ],
  imports: [
    CommonModule,
    TuiCarouselModule
  ],
  exports: [
    BannerComponent
  ]
})
export class BannerModule { }
