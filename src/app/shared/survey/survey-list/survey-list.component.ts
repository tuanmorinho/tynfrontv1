import {Component, inject, OnInit} from '@angular/core';
import {BaseComponent} from "@core/module/base/base.component";
import {SurveyCategoryService, SurveyService} from "@core/services/features";
import {ActivatedRoute} from "@angular/router";
import {ISurvey, ISurveyCategory} from "@core/models/features";

@Component({
    selector: 'app-survey-list',
    templateUrl: './survey-list.component.html'
})
export class SurveyListComponent extends BaseComponent implements OnInit {
    surveyCategoryId!: number;
    surveyCategory!: ISurveyCategory;
    surveyList: ISurvey[] = [];

    private route: ActivatedRoute = inject<ActivatedRoute>(ActivatedRoute);
    private surveyCategoryService: SurveyCategoryService = inject<SurveyCategoryService>(SurveyCategoryService);
    private surveyService: SurveyService = inject<SurveyService>(SurveyService);

    ngOnInit(): void {
        if (this.route.snapshot.params['id']) {
            this.surveyCategoryId = +this.route.snapshot.params['id'];
            this.findAllSurvey();
            this.getDetailCategory();
        }
    }

    private findAllSurvey(): void {
        this.isLoading = true;
        this.surveyService.findAllSurvey({surveyCategoryId: this.surveyCategoryId}).subscribe({
            next: (data: ISurvey[]): void => {
                this.surveyList = data;
                this.isLoading = false;
            }
        });
    }

    private getDetailCategory(): void {
        this.surveyCategoryService.get({id: this.surveyCategoryId}).subscribe({
            next: (data: ISurveyCategory): void => {
                this.surveyCategory = data;
            }
        })
    }
}
