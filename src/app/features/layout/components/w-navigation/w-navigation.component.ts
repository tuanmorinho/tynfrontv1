import {Component, inject, Input, OnInit} from '@angular/core';
import {BaseComponent} from "@core/module/base/base.component";
import {TUI_ARROW} from "@taiga-ui/kit";
import {TranslocoService} from "@ngneat/transloco";
import {last, Observable} from "rxjs";
import {IAccountModel} from "@core/models/core";
import {select} from "@ngrx/store";
import {selectAccount} from "@core/store/auth/auth.selector";
import {TuiDialogContext, TuiDialogService} from "@taiga-ui/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {PolymorpheusContent} from "@tinkoff/ng-polymorpheus";
import {SafeSubscriber} from "rxjs/internal/Subscriber";
import {AuthService} from "@core/services/core";
import {ToastrService} from "ngx-toastr";
import {CustomFormValidators} from "@core/validators/form-validator";

@Component({
    selector: 'app-w-navigation',
    templateUrl: './w-navigation.component.html'
})
export class WNavigationComponent extends BaseComponent implements OnInit {

    availableLangs!: any[];
    activeLang!: string;
    flagCodes: any;
    isRegister: boolean = true;
    isLogin: boolean = false;

    loginForm!: FormGroup;
    registerForm!: FormGroup;

    private fb: FormBuilder = inject<FormBuilder>(FormBuilder);
    private translocoService: TranslocoService = inject<TranslocoService>(TranslocoService);
    private dialog: TuiDialogService = inject<TuiDialogService>(TuiDialogService);
    private authService: AuthService = inject<AuthService>(AuthService);
    private toastr: ToastrService = inject<ToastrService>(ToastrService);

    account$: Observable<IAccountModel | null> = this.store.pipe(select(selectAccount), this.onDestroy<IAccountModel | null>());

    protected readonly TUI_ARROW = TUI_ARROW;

    ngOnInit(): void {
        this.loginForm = this.fb.group({
            mobile: [null, Validators.required],
            password: [null, [Validators.required, CustomFormValidators.passwordValidator]]
        });

        this.registerForm = this.fb.group({
            mobile: [null, Validators.required],
            password: [null, [Validators.required, CustomFormValidators.passwordValidator]],
            fullName: [null, Validators.required]
        });

        this.availableLangs = this.translocoService.getAvailableLangs();

        this.translocoService.langChanges$.subscribe({
            next: (activeLang: string): void => {
                this.activeLang = activeLang;
            }
        });

        this.flagCodes = {
            'en': 'us',
            'vi': 'vi'
        };
    }

    setActiveLang(lang: string): void {
        this.translocoService.setActiveLang(lang);
    }

    logOut(): void {
        this.authService.signOut();
    }

    logInOutDialog(content: PolymorpheusContent<TuiDialogContext<any, string>>): void {
        this.dialog.open(content).subscribe();
    }

    onSubmit(type: 'isRegister' | 'isLogin', observer: SafeSubscriber<any>): void {
        if (type === 'isRegister') {
            this.registerForm.markAllAsTouched();
            if (this.registerForm.controls['password'].hasError('invalidPassword')) {
                this.toastr.error(
                    this.translocoService.translate(
                        'features.layout.components.w-navigation.auth.toastr.password-validate'
                    )
                );
                return;
            }
            if (this.registerForm.invalid) return;
            this.registerForm.disable();
            this.authService.register({...this.registerForm.getRawValue()})
                .pipe(last())
                .subscribe({
                    next: (data: { token: string }): void => {
                        if (data && data.token) {
                            observer.complete();
                        }
                    },
                    error: (err): void => {
                        this.registerForm.enable();
                        this.registerForm.reset();
                        this.toastr.error(err?.error?.message);
                    }
                })
        }
        if (type === 'isLogin') {
            this.loginForm.markAllAsTouched();
            if (this.loginForm.controls['password'].hasError('invalidPassword')) {
                this.toastr.error(
                    this.translocoService.translate(
                        'features.layout.components.w-navigation.auth.toastr.password-validate'
                    )
                );
                return;
            }
            if (this.loginForm.invalid) return;
            this.loginForm.disable();
            this.authService.signIn({...this.loginForm.getRawValue()})
                .pipe(last())
                .subscribe({
                    next: (data: { token: string }): void => {
                        if (data && data.token) {
                            observer.complete();
                        }
                    },
                    error: (err): void => {
                        this.loginForm.enable();
                        this.loginForm.reset();
                        this.toastr.error(err?.error?.message);
                    }
                });
        }
    }
}
