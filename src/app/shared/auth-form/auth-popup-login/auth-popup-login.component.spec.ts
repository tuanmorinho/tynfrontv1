import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthPopupLoginComponent } from './auth-popup-login.component';

describe('AuthPopupLoginComponent', () => {
  let component: AuthPopupLoginComponent;
  let fixture: ComponentFixture<AuthPopupLoginComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AuthPopupLoginComponent]
    });
    fixture = TestBed.createComponent(AuthPopupLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
