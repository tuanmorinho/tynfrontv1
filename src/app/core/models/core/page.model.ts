export interface Pageable {
  page: number;
  take: number;
  total: number;
  sort: {
    empty: boolean;
    unsorted: boolean;
    sorted: boolean
  }
}

export interface Pagination<T> {
  content: Array<T>;
  pageable: Pageable;
}
