import {DOCUMENT} from '@angular/common';
import {inject, Pipe, PipeTransform, Renderer2, SecurityContext} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';

@Pipe({
    name: 'bypass'
})
export class BypassPipe implements PipeTransform {

    private sanitizer: DomSanitizer = inject<DomSanitizer>(DomSanitizer);
    private render2: Renderer2 = inject<Renderer2>(Renderer2);
    private document: Document = inject(DOCUMENT);

    transform(value: string, context: SecurityContext = SecurityContext.HTML) {
        switch (context) {
            case SecurityContext.HTML:
                console.log('fdfsdf')
                const html = this.transformHTML(value);
                return this.sanitizer.bypassSecurityTrustHtml(html || '');
            case SecurityContext.SCRIPT:
                return this.sanitizer.bypassSecurityTrustScript(value || '');
            case SecurityContext.STYLE:
                console.log('fdfsdf')
                return this.sanitizer.bypassSecurityTrustStyle(value || '');
            case SecurityContext.URL:
                return this.sanitizer.bypassSecurityTrustUrl(value || '');
            case SecurityContext.RESOURCE_URL:
                return this.sanitizer.bypassSecurityTrustResourceUrl(value || '');
            default:
                return value;
        }
    }

    transformHTML(value: string) {
        const temp = this.render2.createElement('div');
        this.render2.setProperty(temp, 'innerHTML', value);
        const els: HTMLIFrameElement[] = Array.from(temp.querySelectorAll('iframe[src*="youtube.com"]'));
        els.map(el => {
            el.src = el.src.split('?')[0] + '?enablejsapi=1&autoplay=0&modestbranding=1&origin=' + this.document.location.origin;
        });
        return temp.innerHTML;
    }

}
