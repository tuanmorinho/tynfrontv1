import {AppState} from "../app.state";

export const selectSearchKeyword = (state: AppState) => state.feature_search.keyword;
