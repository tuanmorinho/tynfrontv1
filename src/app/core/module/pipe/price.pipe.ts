import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'price'
})
export class PricePipe implements PipeTransform {

    transform(value: number | undefined): string {
        if (value == undefined) return new Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' }).format(0);
        return new Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' }).format(value * 1000);
    }

}
