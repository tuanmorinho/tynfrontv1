import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MHeaderLeftDirective } from './m-header-left.directive';
import { MHeaderRightDirective } from './m-header-right.directive';
import { MHeaderCenterDirective } from './m-header-center.directive';



@NgModule({
  declarations: [
    MHeaderLeftDirective,
    MHeaderRightDirective,
    MHeaderCenterDirective
  ],
  imports: [
    CommonModule
  ],
  exports: [
    MHeaderLeftDirective,
    MHeaderRightDirective,
    MHeaderCenterDirective
  ]
})
export class DirectiveModule { }
