import {inject, Injectable} from '@angular/core';
import {Meta, Title} from "@angular/platform-browser";
import {DOCUMENT} from "@angular/common";
import {BehaviorSubject, catchError, Observable, of, shareReplay, tap} from "rxjs";
import {RequestSeoMeta, SeoMeta} from "@core/models/core/seo.model";
import {PageService} from "@core/services/core/page.service";

@Injectable({
  providedIn: 'root'
})
export class SeoService {

  private _initSeoMeta$: BehaviorSubject<SeoMeta> = new BehaviorSubject<SeoMeta>({});
  private _seoMeta$: BehaviorSubject<SeoMeta> = new BehaviorSubject<SeoMeta>({});
  seoMeta$: Observable<SeoMeta>;

  private meta: Meta = inject<Meta>(Meta);
  private title: Title = inject<Title>(Title);
  private pageService: PageService = inject<PageService>(PageService);
  private document: Document = inject<Document>(DOCUMENT);

  constructor() {
    this._initSeoMeta$.next({ title: 'Beauty Match | Giải pháp chăm sóc sức khỏe và sắc đẹp', description: 'Giải pháp chăm sóc sức khỏe và sắc đẹp phù hợp' })
    this.seoMeta$ = this._seoMeta$.pipe(shareReplay({bufferSize: 1, refCount: true}));
  }

  updateSeoMeta(data?: SeoMeta): void {
    this.setMeta({...this._initSeoMeta$.value, ...data});
  }

  private setMeta(data: SeoMeta): void {
    if (data) {
      this.updateMetaTag(data);
      this._seoMeta$.next(data);
    }
  }

  private updateMetaTag(data: SeoMeta): void {
    this.title.setTitle(data.title as string);
    this.meta.updateTag({name: 'title', content: data.title as string});
    this.meta.updateTag({name: 'description', content: data.description as string});
  }

  getSeoMeta(body?: RequestSeoMeta): void {
    this.pageService.getSeoMetaConfig(body).pipe(
      tap((data: SeoMeta) => {
        this.updateSeoMeta(data);
      }),
      catchError(() => {
        this.updateSeoMeta();
        return of(null)
      })
    ).subscribe();
  }
}
