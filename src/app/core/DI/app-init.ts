import {Observable, of} from "rxjs";
import {AppState} from "@core/store/app.state";
import {Store} from "@ngrx/store";
import * as UAParser from "ua-parser-js";
import {setDeviceDesktop, setDeviceMobile, setDeviceType} from "@core/store/device/device.actions";
import {CookieService} from "@core/services/core";
import {DeviceUUID} from 'device-uuid';

export function initializeDeviceFactory(
  store: Store<AppState>
): () => Observable<null | undefined> {
  return (): Observable<null | undefined> => {
    const deviceType: string = new UAParser().getDevice()?.type || 'desktop';
    store.dispatch(setDeviceType({payload: deviceType}));
    store.dispatch(setDeviceMobile({payload: deviceType}));
    store.dispatch(setDeviceDesktop({payload: deviceType}));
    return of(null);
  }
}

export function initializeVisitorUuidFactory(
    cookie: CookieService,
): () => Observable<any> {
  return () => {
    const uuid: string = new DeviceUUID().get();
    if (uuid) {
      cookie.set('visitor-uuid', uuid, 50 * 365, '/');
    }
    return of(null);
  };
}
