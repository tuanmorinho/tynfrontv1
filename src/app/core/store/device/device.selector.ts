import {AppState} from "../app.state";

export const selectDevice = (state: AppState) => state.feature_device;
