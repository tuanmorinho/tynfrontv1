import {Component, inject} from '@angular/core';
import {BaseComponent} from "@core/module/base/base.component";
import {
    IDoSurveyQuestionResponse,
    IDoSurveyResponse,
    ISubmitSurveyAnswerRequest,
    ISurvey,
    ISurveyAccount, ISurveyAccountAnswer,
    ISurveyAnswer,
    SURVEY_ANSWER_DISPLAY_TYPE,
    SURVEY_QUESTION_ANSWER_DISPLAY_TYPE,
    SURVEY_QUESTION_ANSWER_SELECT_TYPE
} from "@core/models/features";
import {ActivatedRoute} from "@angular/router";
import {SurveyService} from "@core/services/features";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {delay} from "rxjs";
import {environment} from "../../../../../enviroments";
import {ToastrService} from "ngx-toastr";
import {ROUTING_CONSTANT, ROUTING_TYN} from "@core/constants/routing-constant";

@Component({
    selector: 'app-quiz-page',
    templateUrl: './quiz-page.component.html'
})
export class QuizPageComponent extends BaseComponent {
    surveyAccount: ISurveyAccount | undefined;
    surveyId: number | undefined;
    survey: ISurvey | undefined;
    surveyQuestion: IDoSurveyQuestionResponse | undefined;
    surveyAnswerList: ISurveyAnswer[] | undefined;
    surveyAccountAnswerList: ISurveyAccountAnswer[] | undefined;
    questionStep: number | undefined;
    urlImageBase: string = environment.apiUrl.base.replace('/v2', '');
    isDone: boolean = false;

    form!: FormGroup;

    private fb: FormBuilder = inject<FormBuilder>(FormBuilder);
    private route: ActivatedRoute = inject<ActivatedRoute>(ActivatedRoute);
    private toastr: ToastrService = inject<ToastrService>(ToastrService);
    private surveyService: SurveyService = inject<SurveyService>(SurveyService);

    protected readonly SURVEY_QUESTION_ANSWER_DISPLAY_TYPE = SURVEY_QUESTION_ANSWER_DISPLAY_TYPE;
    protected readonly SURVEY_QUESTION_ANSWER_SELECT_TYPE = SURVEY_QUESTION_ANSWER_SELECT_TYPE;
    protected readonly SURVEY_ANSWER_DISPLAY_TYPE = SURVEY_ANSWER_DISPLAY_TYPE;
    protected readonly Boolean = Boolean;

    ngOnInit(): void {
        this.form = this.fb.group({
            answerIdList: null,
            answerDemandScore: 10,
            answerText: null,
            answerImageUpload1: null,
            answerImageUpload2: null,
            answerImageUpload3: null,
            answerImageUpload4: null,
            answerImageUpload5: null
        });

        if (this.route.snapshot.params['id']) {
            this.surveyId = +this.route.snapshot.params['id'];
            this.doSurvey();
        }
    }

    get formControls() {
        return this.form.controls;
    }

    get formStatus() {
        return this.form.status;
    }

    get canSubmit(): boolean {
        if (this.surveyQuestion?.isAnswerRequired && this.surveyAccountAnswerList) {
            return Boolean(this.surveyAccountAnswerList && this.surveyAccountAnswerList.length > 0);
        }
        return true;
    }

    get canNextQuestion(): boolean {
        if (this.surveyQuestion?.isAnswerRequired) {
            return Boolean(this.surveyAccountAnswerList && this.surveyAccountAnswerList.length > 0);
        }
        return true;
    }

    onToggleSelectAnswer(answer: ISurveyAnswer): void {
        if ([...this.formControls['answerIdList'].value as Array<number | undefined>].includes(answer?.id)) {
            const prevId: Array<number | undefined> = this.formControls['answerIdList'].value as Array<number | undefined>;
            const indexDel: number = [...prevId].indexOf(answer?.id);
            prevId.splice(indexDel, 1)
            this.formControls['answerIdList'].setValue([...prevId]);
            return;
        }

        if (this.surveyQuestion?.answerSelectType === SURVEY_QUESTION_ANSWER_SELECT_TYPE.ANSWER_SELECT_TYPE_ONE.value) {
            this.formControls['answerIdList'].setValue([answer?.id]);
            return;
        }

        if (this.surveyQuestion?.answerSelectType === SURVEY_QUESTION_ANSWER_SELECT_TYPE.ANSWER_SELECT_TYPE_MULTI.value) {
            const prevId: Array<number | undefined> = this.formControls['answerIdList'].value as Array<number | undefined>;
            this.formControls['answerIdList'].setValue([...prevId, answer?.id]);
            return;
        }
    }

    isAnswerSelected(answer: ISurveyAnswer) {
        return this.formControls['answerIdList'].value.includes(answer?.id);
    }

    sliderChange(evt: any) :void {
    }

    onSubmit(): void {
        this.form.markAllAsTouched();
        if (this.form.invalid) return;

        if (this.formControls['answerIdList'].value?.length === 0 && !this.surveyQuestion?.isAnswerRequired) {
            this.nextQuestion(false);
            return;
        }

        const data: ISubmitSurveyAnswerRequest = {
            surveyAccountId: Number(this.surveyAccount?.id),
            surveyId: Number(this.surveyId),
            questionId: Number(this.surveyQuestion?.id),
            stepIndex: Number(this.questionStep),
            answerIdList: this.formControls['answerIdList'].value,
            answerDemandScore: Number(this.formControls['answerDemandScore'].value),
            answerText: this.formControls['answerText'].value,
            answerImageUpload1: this.formControls['answerImageUpload1'].value,
            answerImageUpload2: this.formControls['answerImageUpload2'].value,
            answerImageUpload3: this.formControls['answerImageUpload3'].value,
            answerImageUpload4: this.formControls['answerImageUpload4'].value,
            answerImageUpload5: this.formControls['answerImageUpload5'].value,
        };

        this.isLoading = true;
        this.surveyService.submitAnswer(data)
            .pipe(
                this.onDestroy<IDoSurveyResponse>()
            ).subscribe({
            next: (data: IDoSurveyResponse): void => {
                this.surveyAccount = data?.surveyAccount;
                this.nextQuestion(true);
            },
            error: (err): void => {
                this.toastr.error(err?.error?.message);
                this.isLoading = false;
            }
        })
    }

    prevQuestion(): void {
        this.isLoading = true;
        let data: ISubmitSurveyAnswerRequest = {
            surveyAccountId: Number(this.surveyAccount?.id),
            surveyId: Number(this.surveyId),
            questionId: Number(this.surveyQuestion?.id),
            answerIdList: this.formControls['answerIdList'].value,
            stepIndex: Number(this.questionStep) - 1,
            answerDemandScore: Number(this.formControls['answerDemandScore'].value),
            answerText: this.formControls['answerText'].value,
        };

        if (this.surveyQuestion && !this.surveyQuestion.isAnswerRequired) {
            if (this.surveyAnswerList && this.surveyAnswerList.length) {
                data.answerIdList = this.surveyAnswerList.map((v: ISurveyAnswer) => Number(v.id));
            }
        }

        this.surveyService.getPrevQuestion(data)
            .pipe(
                this.onDestroy<any>()
            ).subscribe({
            next: (data: IDoSurveyResponse): void => {
                this.surveyAccount = data?.surveyAccount;
                this.surveyId = data?.survey?.id;
                this.survey = data?.survey;
                this.questionStep = data?.questionStep;
                this.surveyQuestion = data?.doSurveyQuestionResponse;
                this.surveyAnswerList = data?.doSurveyQuestionResponse?.surveyAnswerList;
                this.surveyAccountAnswerList = data?.doSurveyQuestionResponse?.surveyAccountAnswerList;
                this.updateValidator();
                this.updateAnswered();
                this.isLoading = false;
            },
            error: (err): void => {
                this.toastr.error(err?.error?.message);
                this.isLoading = false;
            }
        })
    }

    nextQuestion(isNextOnSubmit: boolean): void {
        this.form.markAllAsTouched();
        if (this.form.invalid) return;

        let data: ISubmitSurveyAnswerRequest = {
            surveyAccountId: Number(this.surveyAccount?.id),
            surveyId: Number(this.surveyId),
            questionId: Number(this.surveyQuestion?.id),
            answerIdList: this.formControls['answerIdList'].value,
            stepIndex: Number(this.questionStep) + 1,
            answerDemandScore: Number(this.formControls['answerDemandScore'].value),
            answerText: this.formControls['answerText'].value,
        };

        if (!isNextOnSubmit) {
            this.isLoading = true;
            if (this.surveyQuestion && !this.surveyQuestion.isAnswerRequired) {
                if (this.surveyAnswerList && this.surveyAnswerList.length) {
                    data.answerIdList = this.surveyAnswerList.map((v: ISurveyAnswer) => Number(v.id));
                }
            }
        }

        this.surveyService.getNextQuestion(data)
            .pipe(
                this.onDestroy<any>()
            ).subscribe({
            next: (data: IDoSurveyResponse): void => {
                this.surveyAccount = data?.surveyAccount;
                this.surveyId = data?.survey?.id;
                this.survey = data?.survey;
                this.questionStep = data?.questionStep;
                this.surveyQuestion = data?.doSurveyQuestionResponse;
                this.surveyAnswerList = data?.doSurveyQuestionResponse?.surveyAnswerList;
                this.surveyAccountAnswerList = data?.doSurveyQuestionResponse?.surveyAccountAnswerList;
                this.isDone = !!data.done;
                this.updateValidator();
                this.updateAnswered();
                this.isLoading = false;
            },
            error: (err): void => {
                this.toastr.error(err?.error?.message);
                this.isLoading = false;
            }
        })
    }

    redoSurvey(): void {
        this.isLoading = true;
        this.surveyService.redoSurvey({surveyId: this.surveyId})
            .pipe(
                delay(500),
                this.onDestroy<IDoSurveyResponse>()
            )
            .subscribe({
                next: (data: IDoSurveyResponse): void => {
                    this.surveyAccount = data?.surveyAccount;
                    this.surveyId = data?.survey?.id;
                    this.survey = data?.survey;
                    this.questionStep = data?.questionStep;
                    this.surveyQuestion = data?.doSurveyQuestionResponse;
                    this.surveyAnswerList = data?.doSurveyQuestionResponse?.surveyAnswerList;
                    this.isDone = !!data.done;
                    this.updateValidator();
                    this.isLoading = false;
                }
            })
    }

    getProductRecommend(): void {
        if (this.surveyId) {
            void this.router.navigateByUrl(`${ROUTING_TYN.PRODUCT}/${ROUTING_CONSTANT.DETAIL}/${this.surveyId}`);
        }
    }

    private doSurvey(): void {
        this.isLoading = true;
        this.surveyService.doSurvey({surveyId: this.surveyId})
            .pipe(
                delay(500),
                this.onDestroy<IDoSurveyResponse>()
            )
            .subscribe({
                next: (data: IDoSurveyResponse): void => {
                    this.surveyAccount = data?.surveyAccount;
                    this.surveyId = data?.survey?.id;
                    this.survey = data?.survey;
                    this.questionStep = data?.questionStep;
                    this.surveyQuestion = data?.doSurveyQuestionResponse;
                    this.surveyAnswerList = data?.doSurveyQuestionResponse?.surveyAnswerList;
                    this.isDone = !!data.done;
                    this.updateValidator();
                    this.isLoading = false;
                }
            })
    };

    private updateValidator(): void {
        this.resetForm();
        if (this.surveyQuestion && this.surveyQuestion.isAnswerRequired) {
            if (this.surveyAnswerList && this.surveyAnswerList.length) {
                this.formControls['answerIdList'].setValidators([Validators.required]);
                this.formControls['answerIdList'].updateValueAndValidity();
                this.surveyAnswerList?.forEach((answer: ISurveyAnswer): void => {
                    if (answer.displayType === SURVEY_ANSWER_DISPLAY_TYPE.DISPLAY_TYPE_PROGRESS_BAR.value) {
                        this.formControls['answerIdList'].setValue([answer.id]);
                        this.formControls['answerDemandScore'].setValidators([Validators.required]);
                        this.formControls['answerDemandScore'].updateValueAndValidity();
                    }
                    if (answer.displayType === SURVEY_ANSWER_DISPLAY_TYPE.DISPLAY_TYPE_INPUT.value) {
                        this.formControls['answerIdList'].setValue([answer.id]);
                        this.formControls['answerText'].setValidators([Validators.required]);
                        this.formControls['answerText'].updateValueAndValidity();
                    }
                    if (answer.displayType === SURVEY_ANSWER_DISPLAY_TYPE.DISPLAY_TYPE_IMAGE_UPLOAD.value) {
                        this.formControls['answerIdList'].setValue([answer.id]);
                        this.formControls['answerImageUpload1'].setValidators([Validators.required]);
                        this.formControls['answerImageUpload1'].updateValueAndValidity();
                    }
                });
            }
        }
        if (this.surveyAnswerList && this.surveyAnswerList.length) {
            this.surveyAnswerList?.forEach((answer: ISurveyAnswer): void => {
                if (answer.displayType === SURVEY_ANSWER_DISPLAY_TYPE.DISPLAY_TYPE_PROGRESS_BAR.value) {
                    this.formControls['answerIdList'].setValue([answer.id]);
                }
                if (answer.displayType === SURVEY_ANSWER_DISPLAY_TYPE.DISPLAY_TYPE_INPUT.value) {
                    this.formControls['answerIdList'].setValue([answer.id]);
                }
                if (answer.displayType === SURVEY_ANSWER_DISPLAY_TYPE.DISPLAY_TYPE_IMAGE_UPLOAD.value) {
                    this.formControls['answerIdList'].setValue([answer.id]);
                }
            });
        }
    }

    private updateAnswered(): void {
        if (this.surveyAccountAnswerList && this.surveyAccountAnswerList.length) {
            this.surveyAccountAnswerList?.forEach((accountAnswer: ISurveyAccountAnswer): void => {
                this.formControls['answerIdList'].setValue([accountAnswer.surveyAnswerId]);
                if (accountAnswer.answerDisplayType === SURVEY_ANSWER_DISPLAY_TYPE.DISPLAY_TYPE_PROGRESS_BAR.value) {
                    this.formControls['answerDemandScore'].setValue(accountAnswer.answerDemandScore);
                }
                if (accountAnswer.answerDisplayType === SURVEY_ANSWER_DISPLAY_TYPE.DISPLAY_TYPE_INPUT.value) {
                    this.formControls['answerText'].setValue(accountAnswer.answerText);
                }
                if (accountAnswer.answerDisplayType === SURVEY_ANSWER_DISPLAY_TYPE.DISPLAY_TYPE_IMAGE_UPLOAD.value) {
                    this.formControls['answerImageUpload1'].setValue(accountAnswer.answerImageUpload1);
                    this.formControls['answerImageUpload2'].setValue(accountAnswer.answerImageUpload2);
                    this.formControls['answerImageUpload3'].setValue(accountAnswer.answerImageUpload3);
                    this.formControls['answerImageUpload4'].setValue(accountAnswer.answerImageUpload4);
                    this.formControls['answerImageUpload5'].setValue(accountAnswer.answerImageUpload5);
                }
            })
        }
    }

    private resetForm(): void {
        this.form.reset();
        this.formControls['answerIdList'].setValidators(null);
        this.formControls['answerIdList'].updateValueAndValidity();
        this.form.patchValue({
            answerIdList: [],
            answerDemandScore: [10],
        })
    }
}
