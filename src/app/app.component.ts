import {Component, inject, OnInit} from '@angular/core';
import {BaseComponent} from "@core/module/base/base.component";
import {of, switchMap} from "rxjs";
import {IDeviceInfo} from "@core/models/core";
import {Meta} from "@angular/platform-browser";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent extends BaseComponent implements OnInit {

  private meta: Meta = inject<Meta>(Meta);

  ngOnInit(): void {
    this.updateMetaTag();
  }

  private updateMetaTag() {
    this.deviceInfo$.pipe(
      switchMap((deviceInfo: IDeviceInfo) => {
        this.meta.updateTag({
          name: 'viewport',
          content: deviceInfo.isMobile ? 'width=device-width,initial-scale=1,maximum-scale=1,minimum-scale=1,user-scalable=no,viewport-fit=cover' : 'width=1240,shrink-to-fit=no'
        });
        return of(null)
      })
    ).subscribe();
  }
}
