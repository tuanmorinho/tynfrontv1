import {inject, Injectable} from '@angular/core';
import {environment} from "../../../../enviroments";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ISurveyCategory} from "@core/models/features";

@Injectable({
    providedIn: 'root'
})
export class SurveyCategoryService {

    API_URL: string = environment.apiUrl.base + '/survey';

    private http: HttpClient = inject<HttpClient>(HttpClient);

    findAll(): Observable<ISurveyCategory[]> {
        return this.http.get<ISurveyCategory[]>(this.API_URL + '/findAllCategory');
    }

    get(surveyCategory: ISurveyCategory): Observable<ISurveyCategory> {
        return this.http.post<ISurveyCategory>(this.API_URL + '/getCategory', surveyCategory);
    }
}
