import {createAction, props} from "@ngrx/store";
import {AuthState} from "@core/store/auth/auth.state";

export const SetAuthUser: string = '@Auth/SetAuthUser';

export const setAuthUser = createAction(SetAuthUser, props<{payload: AuthState}>());