import {Component, OnInit} from '@angular/core';
import {BaseComponent} from "@core/module/base/base.component";

@Component({
  selector: 'app-account-page',
  templateUrl: './account-page.component.html'
})
export class AccountPageComponent extends BaseComponent implements OnInit {

  ngOnInit(): void {
    this.configMobileHeader({
      title: null,
      showBack: false,
      showMenu: false,
      showSearch: false,
      showSetting: true,
      showCart: true
    });
    this.updateSeoMetaTag({
      title: 'Cá nhân | TYN STAR CINEPLEX'
    })
  }

}
