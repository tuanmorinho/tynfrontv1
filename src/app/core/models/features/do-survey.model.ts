import {ISurvey, ISurveyAccount, ISurveyAccountAnswer, ISurveyAnswer, ISurveyQuestion} from "@core/models/features";


export interface IDoSurveyQuestionResponse extends ISurveyQuestion {
    surveyAnswerList: ISurveyAnswer[];
    surveyAccountAnswerList?: ISurveyAccountAnswer[];
}

export interface IDoSurveyRequest {
    surveyId?: number;
}

export interface IDoSurveyResponse {
    questionStep?: number;
    surveyAccount?: ISurveyAccount;
    survey?: ISurvey;
    doSurveyQuestionResponse?: IDoSurveyQuestionResponse
    done?: boolean;
}

export interface ISubmitSurveyAnswerRequest {
    surveyAccountId?: number;
    surveyId?: number;
    questionId?: number;
    stepIndex?: number;
    answerIdList?: number[];
    answerDemandScore?: number;
    answerText?: string;
    answerImageUpload1?: string;
    answerImageUpload2?: string;
    answerImageUpload3?: string;
    answerImageUpload4?: string;
    answerImageUpload5?: string;
}