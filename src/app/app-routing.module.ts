import { NgModule } from '@angular/core';
import {ExtraOptions, RouterModule, Routes} from '@angular/router';
import * as UAParser from 'ua-parser-js';
import {MainComponent} from "@layout/main/main.component";
import {ROUTING_TYN} from "@core/constants/routing-constant";
import {noAuthGuard} from "@core/guards/no-auth.guard";
import {webPageGuard} from "@core/guards/web-page.guard";
import {mobilePageGuard} from "@core/guards/mobile-page.guard";

const routes: Routes = [
  {
    path: ROUTING_TYN.DEFAULT,
    component: MainComponent,
    canActivate: [noAuthGuard, webPageGuard],
    children: [
      {
        path: ROUTING_TYN.DEFAULT,
        loadChildren: () => import('@pages/home-page/home-page.module').then(m => m.HomePageModule)
      },
      {
        path: ROUTING_TYN.SURVEY,
        loadChildren: () => import('@pages/survey-page/survey-page.module').then(m => m.SurveyPageModule)
      },
      {
        path: ROUTING_TYN.PRODUCT,
        loadChildren: () => import('@pages/product-recommend-page/product-recommend-page.module').then(m => m.ProductRecommendPageModule)
      },
      {
        path: ROUTING_TYN.PERSONAL,
        loadChildren: () => import('@pages/account-page/account-page.module').then(m => m.AccountPageModule)
      }
    ]
  },
  {
    path: 'error',
    canActivate: [webPageGuard],
    loadChildren: () => import('@pages/error-page/error-page.module').then(m => m.ErrorPageModule)
  },
  {
    path: 'mobile',
    canActivate: [mobilePageGuard],
    loadChildren: () => import('@pages/mobile-page/mobile-page.module').then(m => m.MobilePageModule)
  },
  {
    path: '**', redirectTo: 'error', pathMatch: 'full'
  }
];

const deviceType: string = new UAParser().getDevice()?.type || 'desktop';
const routerOptions: ExtraOptions = {
  scrollPositionRestoration: 'enabled',
  anchorScrolling: 'enabled',
  scrollOffset: deviceType === 'mobile' ? [0, 55] : [0, 150],
  onSameUrlNavigation: 'reload',
  paramsInheritanceStrategy: 'always'
};

@NgModule({
  imports: [RouterModule.forRoot(routes, routerOptions)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
