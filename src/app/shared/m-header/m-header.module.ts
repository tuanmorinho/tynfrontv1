import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MHeaderComponent } from './m-header/m-header.component';
import { MHeaderMenuComponent } from './components/m-header-menu/m-header-menu.component';
import { MHeaderTransparentComponent } from './m-header-transparent/m-header-transparent.component';
import {CoreModule} from "@core/module/core.module";
import { MHeaderSearchComponent } from './components/m-header-search/m-header-search.component';
import {TuiInputModule} from "@taiga-ui/kit";
import {TuiSvgModule, TuiTextfieldControllerModule} from "@taiga-ui/core";
import {ReactiveFormsModule} from "@angular/forms";
import {IconModule} from "../icon/icon.module";



@NgModule({
  declarations: [
    MHeaderComponent,
    MHeaderMenuComponent,
    MHeaderTransparentComponent,
    MHeaderSearchComponent
  ],
  imports: [
    CommonModule,
    CoreModule,
    TuiInputModule,
    TuiTextfieldControllerModule,
    TuiSvgModule,
    ReactiveFormsModule,
    IconModule
  ],
  exports: [
    MHeaderComponent,
    MHeaderMenuComponent,
    MHeaderTransparentComponent,
    MHeaderSearchComponent
  ]
})
export class MHeaderModule { }
