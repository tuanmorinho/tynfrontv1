import {SafeHtml} from "@angular/platform-browser";

export interface ISvgIcon {
  viewBox?: string;
  svgInner?: string | SafeHtml;
}
