import {createReducer, on} from "@ngrx/store";
import {SearchState} from "./search.state";
import {setKeywordSearch} from "./search.actions";

const initialState: SearchState = {
  keyword: ''
};

export const searchReducer = createReducer(
  initialState,
  on(setKeywordSearch, (state: SearchState, {payload}) => payload),
);
