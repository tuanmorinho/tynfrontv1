import {APP_INITIALIZER, Provider} from "@angular/core";
import {Translation, TRANSLOCO_CONFIG, TRANSLOCO_LOADER, translocoConfig, TranslocoService} from "@ngneat/transloco";
import {environment} from "../../../enviroments";
import {TranslocoLoaderService} from "@core/services/core/transloco-loader.service";

export const TranslocoFactory: Provider[] = [
  {
    // Provide the default Transloco configuration
    provide : TRANSLOCO_CONFIG,
    useValue: translocoConfig({
      availableLangs: [
        {
          id   : 'vi',
          label: 'Việt Nam'
        },
        {
          id   : 'en',
          label: 'English'
        }
      ],
      defaultLang: 'vi',
      fallbackLang: 'vi',
      reRenderOnLangChange: true,
      prodMode: environment.production
    })
  },
  {
    // Provide the default Transloco loader
    provide: TRANSLOCO_LOADER,
    useClass: TranslocoLoaderService
  },
  {
    // Preload the default language before the app starts to prevent empty/jumping content
    provide: APP_INITIALIZER,
    deps: [TranslocoService],
    useFactory: (translocoService: TranslocoService): any => (): Promise<Translation | undefined> => {
      const defaultLang = translocoService.getDefaultLang();
      translocoService.setActiveLang(defaultLang);
      return translocoService.load(defaultLang).toPromise();
    },
    multi: true
  },
]
