import {Component, inject, OnInit} from '@angular/core';
import {BaseComponent} from "@core/module/base/base.component";

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html'
})
export class HomePageComponent extends BaseComponent implements OnInit {

  ngOnInit(): void {
    this.configMobileHeader({
      title: null,
      showBack: false,
      showMenu: true,
      showSearch: true,
      showSetting: false,
      showCart: true
    });
  }
}
