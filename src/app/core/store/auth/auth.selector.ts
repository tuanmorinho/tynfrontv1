import {AppState} from "@core/store/app.state";

export const selectAccount = (state: AppState) => state.feature_auth.account;
