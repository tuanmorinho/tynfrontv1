import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProductRecommendPageComponent} from './product-recommend-page/product-recommend-page.component';
import {RouterModule, Routes} from "@angular/router";
import {CoreModule} from "@core/module/core.module";
import {SharedModule} from "@shared/shared.module";
import {ROUTING_CONSTANT} from "@core/constants/routing-constant";

const routes: Routes = [
    {
        path: ROUTING_CONSTANT.DETAIL + '/:id',
        component: ProductRecommendPageComponent,
        data: {wShowFooter: false}
    },
]

@NgModule({
    declarations: [
        ProductRecommendPageComponent
    ],
    imports: [
        CommonModule,
        CommonModule,
        RouterModule.forChild(routes),
        CoreModule,
        SharedModule
    ]
})
export class ProductRecommendPageModule {
}
