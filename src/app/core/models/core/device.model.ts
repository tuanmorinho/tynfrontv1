export interface IDeviceInfo {
  deviceType?: string;
  isDesktop?: boolean;
  isMobile?: boolean;
}
