import {Directive, ElementRef, inject} from '@angular/core';

@Directive({
  selector: '[appMHeaderRight]'
})
export class MHeaderRightDirective {

  public el: ElementRef = inject<ElementRef>(ElementRef);

}
