import {BaseModel} from "@core/models/core";
import {ISurveyCategory} from "@core/models/features/survey-category.model";

export interface IProduct extends BaseModel {
    surveyCategory?: ISurveyCategory
    name?: string;
    url?: string;
    price?: number;
    descriptionSummary?: string;
    descriptionUsage?: string;
    descriptionElement?: string;
    descriptionFull?: string;
    imageThumbnail?: string;
}