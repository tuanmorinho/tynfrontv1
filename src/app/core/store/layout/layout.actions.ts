import {createAction, props} from "@ngrx/store";
import {LayoutState} from "./layout.state";

export const SetLayout: string = '@Layout/SetLayout';
export const UpdateLayout: string = '@Layout/UpdateLayout';

export const setLayout = createAction(SetLayout, props<{ payload: LayoutState }>());
export const updateLayout = createAction(UpdateLayout, props<{ payload: LayoutState }>());
