import {ISurveyQuestion} from "@core/models/features/survey-question.model";
import {ISurveyAnswer} from "@core/models/features/survey-answer.model";

export interface ISurveyQuestionStep {
    surveyQuestionId: number;
    surveyQuestion?: ISurveyQuestion;
    surveyAnswer?: ISurveyAnswer[];
    step: Array<{
        surveyAnswerId: number;
        surveyAnswer?: ISurveyAnswer;
        surveyQuestionNextId: number;
        surveyQuestionNext?: ISurveyQuestion;
    }>;
}
