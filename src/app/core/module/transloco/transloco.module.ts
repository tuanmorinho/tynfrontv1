import { NgModule } from '@angular/core';
import {TranslocoFactory} from "@core/DI/transloco-factory";
import {TranslocoModule} from "@ngneat/transloco";



@NgModule({
  exports: [
    TranslocoModule
  ],
  providers: [
    TranslocoFactory
  ]
})
export class TranslocoCoreModule { }
