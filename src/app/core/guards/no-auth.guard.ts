import {Observable, of} from "rxjs";
import {inject} from "@angular/core";
import {switchMap} from "rxjs/operators";
import {AuthService} from "@core/services/core";

export const noAuthGuard = (): Observable<boolean> => {
    const authService: AuthService = inject<AuthService>(AuthService);

    return authService.checkAuth().pipe(
        switchMap((authenticated: boolean): Observable<boolean> => {
            if (!authenticated) {
                authService.signOut();
            }
            return of(true);
        })
    )
}