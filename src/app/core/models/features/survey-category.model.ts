import {ISurvey} from "@core/models/features/survey.model";

export interface ISurveyCategoryBase {
  id?: number;
  name?: string;
  description?: string;
  imageThumbnail?: string;
  imageFeature?: string;
  priority?: number;
}

export interface ISurveyCategory extends ISurveyCategoryBase {
}
