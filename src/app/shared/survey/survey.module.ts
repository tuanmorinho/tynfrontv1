import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SurveyListComponent} from './survey-list/survey-list.component';
import {CoreModule} from "@core/module/core.module";
import {IconModule} from "@shared/icon/icon.module";
import {TuiSvgModule} from "@taiga-ui/core";
import { SurveyCardItemComponent } from './survey-card-item/survey-card-item.component';
import {RouterLink, RouterModule} from "@angular/router";


@NgModule({
    declarations: [
        SurveyListComponent,
        SurveyCardItemComponent
    ],
    imports: [
        CommonModule,
        CoreModule,
        IconModule,
        TuiSvgModule,
        RouterModule
    ],
    exports: [
        SurveyListComponent
    ]
})
export class SurveyModule {
}
