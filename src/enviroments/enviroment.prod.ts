export const API_BASE = 'http://localhost:8100/api/survey-recommender/v2';

export const environment = {
  production: true,

  apiUrl: {
    base: API_BASE,
    // notification: API_BASE + '/api/notification',
  },
};
