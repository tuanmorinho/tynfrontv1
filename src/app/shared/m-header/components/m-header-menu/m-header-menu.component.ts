import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'app-m-header-menu',
  templateUrl: './m-header-menu.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MHeaderMenuComponent {

}
