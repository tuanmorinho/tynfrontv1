import {createReducer, on} from "@ngrx/store";
import {LoadingState} from "./loading.state";
import {setLoading, setSkeleton} from "./loading.actions";

const initialState: LoadingState = {
  isLoading: false,
  skeletonVisible: true,
  overlay: false
};

export const loadingReducer = createReducer(
  initialState,
  on(setLoading, (state: LoadingState, { payload }) => ({...state, ...payload})),
  on(setSkeleton, (state: LoadingState, { payload }) => ({...state, skeletonVisible: payload}))
);
