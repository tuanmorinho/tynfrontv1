import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IconModule} from "@shared/icon/icon.module";
import {LoaderModule} from "@shared/loader/loader.module";
import {MHeaderModule} from "@shared/m-header/m-header.module";
import {BannerModule} from "@shared/banner/banner.module";
import {InfiniteScrollModule} from "@shared/infinite-scroll/infinite-scroll.module";
import {ButtonModule} from "@shared/components/button/button.module";
import {SurveyCategoryModule} from "@shared/survey-category/survey-category.module";
import {SurveyModule} from "@shared/survey/survey.module";


@NgModule({
    declarations: [],
    imports: [
        CommonModule
    ],
    exports: [
        IconModule,
        LoaderModule,
        MHeaderModule,
        BannerModule,
        InfiniteScrollModule,
        ButtonModule,
        SurveyCategoryModule,
        SurveyModule
    ]
})
export class SharedModule {
}
