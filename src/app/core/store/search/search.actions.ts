import {createAction, props} from "@ngrx/store";
import {SearchState} from "./search.state";

export const SetKeywordSearch: string = '@Search/SetKeywordSearch';

export const setKeywordSearch = createAction(SetKeywordSearch, props<{payload: SearchState}>());
