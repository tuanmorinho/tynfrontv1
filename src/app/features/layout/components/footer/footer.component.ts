import { Component } from '@angular/core';
import {BaseComponent} from "@core/module/base/base.component";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html'
})
export class FooterComponent extends BaseComponent {

}
