import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ErrorPageComponent} from './error-page.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "@shared/shared.module";
import {CoreModule} from "@core/module/core.module";

const routes: Routes = [
    {
        path: '',
        component: ErrorPageComponent
    }
]

@NgModule({
    declarations: [
        ErrorPageComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedModule,
        CoreModule
    ]
})
export class ErrorPageModule {
}
