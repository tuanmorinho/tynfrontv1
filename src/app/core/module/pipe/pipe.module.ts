import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ToStringListWithCommaPipe} from './to-string-list-with-comma.pipe';
import {PricePipe} from './price.pipe';
import {BypassPipe} from './bypass.pipe';


@NgModule({
    declarations: [
        ToStringListWithCommaPipe,
        PricePipe,
        BypassPipe
    ],
    imports: [
        CommonModule
    ],
    exports: [
        ToStringListWithCommaPipe,
        PricePipe,
        BypassPipe
    ]
})
export class PipeModule {
}
