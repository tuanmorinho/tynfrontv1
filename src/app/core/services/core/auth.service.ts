import {inject, Injectable} from '@angular/core';
import {environment} from 'enviroments';
import {HttpClient} from "@angular/common/http";
import {Observable, of} from "rxjs";
import {switchMap} from "rxjs/operators";
import {IAuthUser} from "@core/models/core";
import {AuthUtils} from "@core/utils/auth.util";
import {jwtDecode} from "jwt-decode";
import {Store} from "@ngrx/store";
import {AppState} from "@core/store/app.state";
import {setAuthUser} from "@core/store/auth/auth.actions";

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    private url: string = environment.apiUrl.base + '/account';

    private http: HttpClient = inject<HttpClient>(HttpClient);
    private store: Store<AppState> = inject<Store<AppState>>(Store<AppState>);

    get tokenLocalStorage(): string | null {
        return localStorage.getItem('tk');
    };

    register(credentials: { username: string; password: string, fullName: string }): Observable<{ token: string }> {
        return this.http.post<{ token: string }>(`${this.url}/register`, credentials).pipe(
            switchMap((res: { token: string }) => {
                const decoded: IAuthUser = jwtDecode(res.token);
                this.setAuthFromLocalStorage(res);
                this.store.dispatch(setAuthUser({payload: decoded}));
                return of(res);
            })
        );
    }

    signIn(credentials: { username: string; password: string }): Observable<{ token: string }> {
        return this.http.post<{ token: string }>(`${this.url}/login`, credentials).pipe(
            switchMap((res: { token: string }) => {
                const decoded: IAuthUser = jwtDecode(res.token);
                this.setAuthFromLocalStorage(res);
                this.store.dispatch(setAuthUser({payload: decoded}));
                return of(res);
            })
        );
    }

    signOut(): Observable<boolean> {
        localStorage.removeItem('tk');
        localStorage.removeItem('us');
        this.store.dispatch(setAuthUser({payload: {account: null}}));
        return of(true);
    }

    getUserByToken(): IAuthUser | boolean | undefined {
        if (!this.tokenLocalStorage) {
            return false;
        }

        const userFromToken: IAuthUser = jwtDecode(this.tokenLocalStorage);
        if (!userFromToken) {
            return undefined;
        }

        this.store.dispatch(setAuthUser({payload: userFromToken}));
        return userFromToken;
    }

    checkAuth(): Observable<boolean> {
        if (!this.tokenLocalStorage) {
            return of(false);
        }

        if (AuthUtils.isTokenExpired(this.tokenLocalStorage)) {
            return of(false);
        }

        if (this.getUserByToken()) {
            return of(true);
        }

        return of(false);
    }

    private setAuthFromLocalStorage(auth: { token: string }): void {
        const decoded: IAuthUser = jwtDecode(auth.token);
        localStorage.setItem('tk', auth.token);
        localStorage.setItem('us', JSON.stringify(decoded));
    }
}
