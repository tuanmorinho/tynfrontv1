import {APP_BASE_HREF, PathLocationStrategy, PlatformLocation} from "@angular/common";
import {Inject, inject, Injectable, Optional} from "@angular/core";
import {select, Store} from "@ngrx/store";
import {AppState} from "../../store/app.state";
import {Observable} from "rxjs";
import {IDeviceInfo} from "../../models/core";
import {selectDevice} from "../../store/device/device.selector";

@Injectable({
  providedIn: 'root'
})
export class LocationStrategyService extends PathLocationStrategy {
  private store: Store<AppState> = inject<Store<AppState>>(Store<AppState>);
  private deviceInfo$: Observable<IDeviceInfo> = this.store.pipe(select(selectDevice));
  private deviceInfo!: IDeviceInfo;

  constructor(
    @Inject(PlatformLocation) _platformLocation: PlatformLocation,
    @Inject(APP_BASE_HREF) @Optional() href?: string
  ) {
    super(_platformLocation, href);
    this.deviceInfo$.subscribe(deviceInfo => this.deviceInfo = deviceInfo);
  }

  override pushState(state: any, title: string, url: string, queryParams: string) {
    if (this.deviceInfo.isMobile) {
      this._mobilePushState(state, title, url, queryParams);
    } else {
      super.pushState(state, title, url, queryParams);
    }
  }

  getPathFromUrl(url: string) {
    return url.split(/[?#]/)[0];
  }

  private _mobilePushState(state: any, title: string, url: string, queryParams: string) {
    const curPathName = this.getPathFromUrl(this.path());
    const nextPathName = this.getPathFromUrl(url);
    if (curPathName === nextPathName) {
      super.replaceState(state, title, url, queryParams);
    } else {
      super.pushState(state, title, url, queryParams);
    }
  }

}
