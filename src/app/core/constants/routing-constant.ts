export const ROUTING_TYN = {
  DEFAULT: '',
  PERSONAL: 'ca-nhan',
  SETTINGS: 'cai-dat',
  SURVEY: 'khao-sat',
  PRODUCT: 'san-pham-goi-y'
}

export const ROUTING_CONSTANT = {
  DEFAULT: '',
  LIST: 'danh-sach',
  DETAIL: 'chi-tiet',
  QUIZ: 'quiz'
}
