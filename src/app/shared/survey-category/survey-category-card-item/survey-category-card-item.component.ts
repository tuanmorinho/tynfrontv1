import {Component, Input} from '@angular/core';
import {BaseComponent} from "@core/module/base/base.component";
import {ISurveyCategory} from "@core/models/features";
import {environment} from "../../../../enviroments";
import {ROUTING_CONSTANT, ROUTING_TYN} from "@core/constants/routing-constant";

@Component({
    selector: 'app-survey-category-card-item',
    templateUrl: './survey-category-card-item.component.html'
})
export class SurveyCategoryCardItemComponent extends BaseComponent {
    @Input() surveyCategory!: ISurveyCategory;

    urlImageBase: string = environment.apiUrl.base.replace('/v2', '');

    navigateToSurveyList(): void {
        if (this.surveyCategory && this.surveyCategory.id) {
            void this.router.navigate([`${ROUTING_TYN.SURVEY}/${ROUTING_CONSTANT.LIST}/${this.surveyCategory.id}`])
        }
    }
}
