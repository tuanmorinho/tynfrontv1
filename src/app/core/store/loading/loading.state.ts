export interface LoadingState {
  isLoading?: boolean;
  skeletonVisible?: boolean;
  overlay?: boolean;
}
