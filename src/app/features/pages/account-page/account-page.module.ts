import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountPageComponent } from './account-page.component';
import {RouterModule, Routes} from "@angular/router";
import {CoreModule} from "@core/module/core.module";
import {SharedModule} from "@shared/shared.module";

const routes: Routes = [
  {
    path: '',
    component: AccountPageComponent
  }
]

@NgModule({
  declarations: [
    AccountPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    CoreModule,
    SharedModule
  ]
})
export class AccountPageModule { }
