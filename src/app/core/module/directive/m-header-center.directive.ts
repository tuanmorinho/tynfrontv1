import {Directive, ElementRef, inject} from '@angular/core';

@Directive({
  selector: '[appMHeaderCenter]'
})
export class MHeaderCenterDirective {

  public el: ElementRef = inject<ElementRef>(ElementRef);

}
