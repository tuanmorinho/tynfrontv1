import {inject, Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {catchError, Observable, throwError} from 'rxjs';
import {AuthService} from "@core/services/core/auth.service";
import {Router} from "@angular/router";
import {AuthUtils} from "@core/utils/auth.util";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    private authService: AuthService = inject<AuthService>(AuthService);
    private router: Router = inject<Router>(Router);

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let newRequest: HttpRequest<any>;
        const token: string | null = localStorage.getItem('tk');

        if (token && !AuthUtils.isTokenExpired(token)) {
            newRequest = request.clone({
                withCredentials: true,
                headers: request.headers
                    .set('Authorization', 'Bearer ' + token)
            });
        } else {
            newRequest = request.clone({
                withCredentials: true
            });
        }

        return next.handle(newRequest).pipe(
            catchError(err => {
                if (err instanceof HttpErrorResponse && err.status === 401) {
                    this.authService.signOut();
                    void this.router.navigateByUrl('/');
                }
                return throwError(err);
            })
        );
    }
}
