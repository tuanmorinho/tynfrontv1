import {animate, AUTO_STYLE, style, transition, trigger} from '@angular/animations';

export const triggerEnterFade = (timings: string | number = '100ms ease') => trigger('triggerEnterFade', [
  transition(':enter', [
    style({opacity: 0}),
    animate(timings, style({opacity: 1}))
  ])
]);
export const triggerLeaveFade = (timings: string | number = '100ms ease') => trigger('triggerLeaveFade', [
  transition(':leave', [
    style({opacity: 1}),
    animate(timings, style({opacity: 0}))
  ])
]);
