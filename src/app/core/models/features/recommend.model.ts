import {IProduct} from "@core/models/features/product.model";

export interface IRecommendResponse {
    product?: IProduct
}

export interface IRecommendRequest {
    surveyId: number;
}