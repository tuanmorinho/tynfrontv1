export interface ISurveyAnswerBase {
    id?: number;
    surveyQuestionId?: number;
    name?: string;
    description?: string;
    imageThumbnail?: string;
    imageFeature?: string;
    displayType?: number;
    priority?: number;
    demandScore?: number;
    price?: number;
}

export interface ISurveyAnswer extends ISurveyAnswerBase {

}

export interface ISurveyAnswerRequest extends ISurveyAnswerBase {
}

export const SURVEY_ANSWER_DISPLAY_TYPE = {
    DISPLAY_TYPE_SELECT: {value: 1, label: 'Kiểu chọn (chọn một hoặc chọn nhiều)'},
    DISPLAY_TYPE_INPUT: {value: 2, label: 'Kiểu nhập text'},
    DISPLAY_TYPE_IMAGE_UPLOAD: {value: 3, label: 'Kiểu upload ảnh'},
    DISPLAY_TYPE_PROGRESS_BAR: {value: 4, label: 'Kiểu thanh kéo'}
}

export const SURVEY_ANSWER_DISPLAY_TYPE_LIST = [
    SURVEY_ANSWER_DISPLAY_TYPE.DISPLAY_TYPE_SELECT,
    SURVEY_ANSWER_DISPLAY_TYPE.DISPLAY_TYPE_INPUT,
    SURVEY_ANSWER_DISPLAY_TYPE.DISPLAY_TYPE_IMAGE_UPLOAD,
    SURVEY_ANSWER_DISPLAY_TYPE.DISPLAY_TYPE_PROGRESS_BAR,
]
