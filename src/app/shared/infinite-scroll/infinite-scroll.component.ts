import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter, inject,
  Input,
  OnDestroy,
  OnInit,
  Output,
  PLATFORM_ID,
  ViewChild
} from '@angular/core';
import {isPlatformBrowser} from '@angular/common';
import {BaseComponent} from "@core/module/base/base.component";
import {Observable, take} from "rxjs";
import {select} from "@ngrx/store";
import {selectLayoutEls} from "@core/store/layout/layout.selector";

@Component({
    selector: 'app-infinite-scroll',
    templateUrl: './infinite-scroll.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class InfiniteScrollComponent extends BaseComponent implements OnInit, OnDestroy, AfterViewInit {

    @Input() options: IntersectionObserverInit = { root: null };
    @Input() container: 'nearest' | 'body' | string = 'body';
    @Output() scrolled: EventEmitter<any> = new EventEmitter();
    @ViewChild('anchor', {static: true}) anchor: ElementRef<HTMLElement> | undefined = undefined;

    private observer: IntersectionObserver | undefined = undefined;

    private platformId: Object = inject<Object>(PLATFORM_ID);
    private host: ElementRef = inject<ElementRef>(ElementRef);

    private layoutConfig$: Observable<Array<boolean | undefined>> = this.store.pipe(
      select(selectLayoutEls(['mHideNav'])),
      this.onDestroy<Array<boolean | undefined>>());

    get element() {
        return this.host?.nativeElement;
    }

    ngOnInit() {
      // this.configScroll();
    }

    ngOnDestroy() {
      if (this.observer) this.observer.disconnect();
    }

    ngAfterViewInit() {
      this.configScroll();
    }

  private configScroll() {
      this.layoutConfig$.pipe(take(1)).subscribe(config => {
        if (isPlatformBrowser(this.platformId)) {
          const options: IntersectionObserverInit = {
            root: this.getContainer(),
            rootMargin: config[0] ? '0px 0px 0px 0px' : '0px 0px -76px 0px',
            ...this.options
          };
          this.observer = new IntersectionObserver(([entry]) => {
            entry.isIntersecting && this.scrolled.emit()
          }, options);
          if (this.anchor) this.observer.observe(this.anchor.nativeElement);
        }
      })
    }

    private isHostScrollable() {
        const style = window.getComputedStyle(this.element);

        return style.getPropertyValue('overflow') === 'auto' ||
            style.getPropertyValue('overflow-y') === 'scroll';
    }

    private isElmScrollable(elm: HTMLElement) {
        const style = window.getComputedStyle(elm);
        const scroll = ['auto', 'scroll', 'overlay'];
        return scroll.includes(style.getPropertyValue('overflow')) || scroll.includes(style.getPropertyValue('overflow-y'));
    }

    private getContainer() {
        if (this.container === 'nearest') {
            let elm: HTMLElement | null = this.element as HTMLElement;
            while (!this.isElmScrollable(elm)) {
                elm = elm.parentElement;
                if (!elm) {
                    return null;
                }
            }
            return elm;
        } else if (this.container === 'body') {
            return null;
        } else {
            return this.element.closest(this.container);
        }
    }
}
