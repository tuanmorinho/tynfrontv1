import {inject, Injectable} from '@angular/core';
import {environment} from "../../../../enviroments";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {IDoSurveyRequest, IDoSurveyResponse, ISubmitSurveyAnswerRequest, ISurvey} from "@core/models/features";

@Injectable({
    providedIn: 'root'
})
export class SurveyService {

    API_URL: string = environment.apiUrl.base + '/survey';

    private http: HttpClient = inject<HttpClient>(HttpClient);

    findAllSurvey(survey: ISurvey): Observable<ISurvey[]> {
        return this.http.post<ISurvey[]>(this.API_URL + '/findAllSurvey', survey);
    }

    getSurvey(survey: ISurvey): Observable<ISurvey> {
        return this.http.post<ISurvey>(this.API_URL + '/getSurvey', survey);
    }

    doSurvey(doSurveyRequest: IDoSurveyRequest): Observable<IDoSurveyResponse> {
        return this.http.post<IDoSurveyResponse>(this.API_URL + '/doSurvey', doSurveyRequest);
    }

    redoSurvey(doSurveyRequest: IDoSurveyRequest): Observable<IDoSurveyResponse> {
        return this.http.post<IDoSurveyResponse>(this.API_URL + '/redoSurvey', doSurveyRequest);
    }

    submitAnswer(submitSurveyAnswerRequest: ISubmitSurveyAnswerRequest): Observable<IDoSurveyResponse> {
        return this.http.post<IDoSurveyResponse>(this.API_URL + '/submitAnswer', submitSurveyAnswerRequest);
    }

    getPrevQuestion(submitSurveyAnswerRequest: ISubmitSurveyAnswerRequest): Observable<IDoSurveyResponse> {
        return this.http.post<IDoSurveyResponse>(this.API_URL + '/getPrevQuestion', submitSurveyAnswerRequest);
    }

    getNextQuestion(submitSurveyAnswerRequest: ISubmitSurveyAnswerRequest): Observable<IDoSurveyResponse> {
        return this.http.post<IDoSurveyResponse>(this.API_URL + '/getNextQuestion', submitSurveyAnswerRequest);
    }
}
